#ifndef LIBJOIS_REGISTRY_H
#define LIBJOIS_REGISTRY_H

#include "libjois/types.h"

#include <cstddef>
#include <memory>
#include <string>
#include <variant>
#include <vector>

namespace jois
{

class DataInputStream;
struct ArrayDesc;
struct ClassDesc;
struct ObjectDesc;

/// I collect and own all objects retrieved from an object stream.
///
/// Each entry is assigned a handle by insert().
///
/// Entries which are initially empty (i.e. null) may be given a value
/// *once* by calling define().
class Registry
{
public:
    /// Read primitive data stored as block data.
    template<typename T>
    static T readPrimitive(DataInputStream& is);

    /// Read one content production from the given DataInputStream.
    ///
    /// @param is A DataInputStream.
    /// @return The result of parsing the content production.
    ObjectValue readContent(DataInputStream& is);

    /// Read an expected content production from the given DataInputStream.
    ///
    /// @param is A DataInputStream.
    /// @return The result of parsing the content production as the given
    ///         type.
    /// @throws std::bad_variant_access if the parsing yielded a different
    ///         type.
    template<typename T>
    T read(DataInputStream& is)
    {
        ObjectValue const result{readContent(is)};
        if (std::holds_alternative<Null>(result)) {
            return T{};
        }
        return std::get<T>(result);
    }

private:
    using Entry = std::variant<
        std::unique_ptr<ArrayDesc>,
        std::unique_ptr<ClassDesc>,
        std::unique_ptr<ObjectDesc>,
        std::unique_ptr<std::string>>;

    /// @param entry An entry.
    /// @return Whether the entry is defined, i.e. it contains a non-null
    ///         pointer.
    static bool isDefined(Entry const& entry);
    static std::size_t index(Integer const handle);

    Integer insert(Entry&& value);
    void define(Integer const handle, Entry&& value);
    ObjectValue at(Integer const handle) const;

private:
    std::vector<Entry> mEntries;
};

template<>
Byte Registry::readPrimitive<Byte>(DataInputStream& is);

template<>
Short Registry::readPrimitive<Short>(DataInputStream& is);

template<>
Integer Registry::readPrimitive<Integer>(DataInputStream& is);

template<>
Long Registry::readPrimitive<Long>(DataInputStream& is);

} // namespace jois

#endif // LIBJOIS_REGISTRY_H
