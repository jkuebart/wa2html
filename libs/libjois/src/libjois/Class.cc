#include "libjois/Class.h"

#include "libjois/DataInputStream.h"
#include "libjois/ObjectStreamConstants.h"
#include "libjois/Registry.h"

#include <algorithm>
#include <iomanip>
#include <iterator>
#include <ostream>
#include <stdexcept>
#include <string>

namespace jois
{

namespace
{

    std::string
    readType(DataInputStream& is, Registry& registry, Byte const typecode)
    {
        switch (typecode) {
        case 'B':
        case 'C':
        case 'D':
        case 'F':
        case 'I':
        case 'J':
        case 'S':
        case 'Z':
            return std::string(1, static_cast<char>(typecode));

        case 'L':
        case '[':
            return *registry.read<String>(is);

        default:
            throw std::runtime_error{
                "Unkwnown typecode: " + std::to_string(typecode)};
        }
    }

} // namespace

ClassDesc::FieldDesc::FieldDesc(DataInputStream& is, Registry& registry)
: typecode{is.read<Byte>()}
, name{is.read<std::string>()}
, type{readType(is, registry, typecode)}
{}

std::ostream& operator<<(
    std::ostream& os,
    ClassDesc::FieldDesc const& fieldDesc);
std::ostream& operator<<(
    std::ostream& os,
    ClassDesc::FieldDesc const& fieldDesc)
{
    return os << "FieldDesc{name=" << std::quoted(fieldDesc.name)
              << " type=" << std::quoted(fieldDesc.type) << '}';
}

namespace
{

    std::vector<ClassDesc::FieldDesc> readFields(
        DataInputStream& is,
        Registry& registry)
    {
        std::vector<ClassDesc::FieldDesc> fields{};
        for (auto count{is.read<Short>()}; 0 != count; --count) {
            fields.emplace_back(is, registry);
        }

        if (ObjectStreamConstants::TC_ENDBLOCKDATA != is.read<Byte>()) {
            throw std::runtime_error{"Unsupported class annotation"};
        }

        return fields;
    }

} // namespace

ClassDesc::ClassDesc(DataInputStream& is, Registry& registry)
: name{is.read<std::string>()}
, serialVersionUID{is.read<Long>()}
, classDescFlags{is.read<Byte>()}
, fields{readFields(is, registry)}
, superClass{registry.read<Class>(is)}
{}

ClassDesc::ClassId ClassDesc::classId() const
{
    return {name, serialVersionUID};
}

std::size_t ClassDesc::numberOfFields() const
{
    return fields.size() + (superClass ? superClass->numberOfFields() : 0);
}

std::size_t ClassDesc::fieldIndex(std::string_view const& fieldName) const
{
    auto const it{std::find_if(
        fields.begin(),
        fields.end(),
        [&fieldName](auto const& field)
        {
            return field.name == fieldName;
        })};

    // If the field exists in this class, ignore superclass.
    if (fields.end() != it) {
        return static_cast<std::size_t>(std::distance(fields.begin(), it)) +
            (superClass ? superClass->numberOfFields() : 0);
    }

    // If there is no superclass, we're finished.
    if (!superClass) {
        throw std::out_of_range{
            "Unknown field name: \"" + std::string{fieldName} + "\""};
    }

    // If there is a superclass, search there.
    return superClass->fieldIndex(fieldName);
}

std::ostream& operator<<(std::ostream& os, Class const& clazz)
{
    if (!clazz) {
        return os << "null";
    }

    os << "Class{name=" << std::quoted(clazz->name)
       << " serialVersionUID=" << clazz->serialVersionUID
       << " classDescFlags=" << static_cast<int>(clazz->classDescFlags)
       << " fields=[";
    char const* sep{""};
    for (auto const& field : clazz->fields) {
        os << sep << field;
        sep = " ";
    }
    return os << "] superClass=" << clazz->superClass << '}';
}

} // namespace jois
