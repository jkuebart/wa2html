#include "libjois/JavaValue.h"

#include "libjois/Array.h"
#include "libjois/Object.h"

#include <iostream>

namespace jois
{

std::ostream& operator<<(std::ostream& os, String const& string)
{
    return os << (string ? *string : "null");
}

Class JavaValue::getClass() const
{
    return (
        std::holds_alternative<Array>(mValue)
            ? std::get<Array>(mValue)->clazz
            : std::get<Object>(mValue)->clazz);
}

JavaValue JavaValue::operator[](std::string_view const& name) const
{
    return std::get<Object>(mValue)->at(name);
}

std::ostream& operator<<(std::ostream& os, JavaValue const& value)
{
    visit(
        [&os](auto const& v)
        {
            os << std::boolalpha << v;
        },
        value);
    return os;
}

} // namespace jois
