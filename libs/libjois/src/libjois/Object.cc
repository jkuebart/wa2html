#include "libjois/Object.h"

#include "libjois/Class.h"
#include "libjois/DataInputStream.h"
#include "libjois/ObjectStreamConstants.h"
#include "libjois/Registry.h"

#include <ostream>
#include <stdexcept>
#include <string>
#include <utility>
#include <variant>

namespace jois
{

namespace
{

    constexpr ClassDesc::ClassId kArrayListClassId{
        "java.util.ArrayList",
        8683452581122892189L};

    std::vector<JavaValue>
    readValues(DataInputStream& is, Registry& registry, Class const& clazz)
    {
        if (!(ObjectStreamConstants::SC_SERIALIZABLE & clazz->classDescFlags)) {
            throw std::runtime_error{
                "Unimplemented classDescFlags=" +
                std::to_string(clazz->classDescFlags)};
        }

        std::vector<JavaValue> values;
        if (clazz->superClass) {
            values = readValues(is, registry, clazz->superClass);
        }
        for (auto const& field : clazz->fields) {
            if ('B' == field.typecode) {
                values.emplace_back(is.read<Byte>());
            } else if ('I' == field.typecode) {
                values.emplace_back(is.read<Integer>());
            } else if ('J' == field.typecode) {
                values.emplace_back(is.read<Long>());
            } else if ('S' == field.typecode) {
                values.emplace_back(is.read<Short>());
            } else if ('F' == field.typecode) {
                values.emplace_back(is.read<float>());
            } else if ('Z' == field.typecode) {
                values.emplace_back(is.read<bool>());
            } else if ("Ljava/lang/String;" == field.type) {
                values.emplace_back(registry.read<String>(is));
            } else if ('L' == field.typecode) {
                values.emplace_back(registry.read<Object>(is));
            } else if ('[' == field.typecode) {
                values.emplace_back(registry.read<Array>(is));
            } else {
                throw std::runtime_error{"Unsupported type " + field.type};
            }
        }
        return values;
    }

    std::vector<JavaValue> readAnnotation(
        DataInputStream& is,
        Registry& registry,
        Class const& clazz,
        std::vector<JavaValue> const& values)
    {
        // If the class doesn't have a writeObject() method, we're done.
        if (!(ObjectStreamConstants::SC_WRITE_METHOD & clazz->classDescFlags)) {
            return {};
        }

        // Read data written by certain writeObject() methods.
        std::vector<JavaValue> annotation{};
        if (kArrayListClassId == clazz->classId()) {
            // see
            // https://github.com/openjdk/jdk/blob/343ecd806bb0/src/java.base/share/classes/java/util/ArrayList.java#L883
            Registry::readPrimitive<Integer>(is);
            for (auto count{get<Integer>(values.at(0))}; 0 != count; --count) {
                auto const content{registry.readContent(is)};
                std::visit(
                    [&annotation](auto const& v)
                    {
                        annotation.emplace_back(v);
                    },
                    content);
            }
        }

        auto const endblockdata{is.read<Byte>()};
        if (ObjectStreamConstants::TC_ENDBLOCKDATA != endblockdata) {
            throw std::runtime_error{
                "Unimplemented " + clazz->name +
                "::readObject() serialVersionUID=" +
                std::to_string(clazz->serialVersionUID)};
        }
        return annotation;
    }

} // namespace

ObjectDesc::ObjectDesc(
    DataInputStream& is,
    Registry& registry,
    Class const& aClass)
: clazz{aClass}
, values{readValues(is, registry, aClass)}
, annotation{readAnnotation(is, registry, aClass, values)}
{}

JavaValue const& ObjectDesc::at(std::string_view const& name) const
{
    return values[clazz->fieldIndex(name)];
}

std::ostream& operator<<(std::ostream& os, Object const& object)
{
    if (!object) {
        return os << "null";
    }
    os << object->clazz->name << "{values=[";
    char const* sep{""};
    for (auto const& field : object->values) {
        os << sep << field;
        sep = " ";
    }
    if (!object->annotation.empty()) {
        os << "] annotation=[";
        sep = "";
        for (auto const& field : object->annotation) {
            os << sep << field;
            sep = " ";
        }
    }
    return os << "]}";
}

} // namespace jois
