#ifndef LIBJOIS_OBJECTSTREAMCONSTANTS_H
#define LIBJOIS_OBJECTSTREAMCONSTANTS_H

#include "libjois/types.h"

namespace jois
{

namespace ObjectStreamConstants
{

    constexpr Short STREAM_MAGIC{static_cast<Short>(0xaced)};
    constexpr Short STREAM_VERSION{5};

    constexpr Byte TC_NULL{'p'};
    constexpr Byte TC_REFERENCE{'q'};
    constexpr Byte TC_CLASSDESC{'r'};
    constexpr Byte TC_OBJECT{'s'};
    constexpr Byte TC_STRING{'t'};
    constexpr Byte TC_ARRAY{'u'};
    constexpr Byte TC_BLOCKDATA{'w'};
    constexpr Byte TC_ENDBLOCKDATA{'x'};

    constexpr Byte SC_WRITE_METHOD{0x1};
    constexpr Byte SC_SERIALIZABLE{0x2};

    constexpr Integer basicWireHandle{0x7e0000};

} // namespace ObjectStreamConstants
} // namespace jois

#endif // LIBJOIS_OBJECTSTREAMCONSTANTS_H
