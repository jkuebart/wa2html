#include "libjois/DataInputStream.h"

#include <cmath>
#include <istream>

namespace jois
{

template<>
bool DataInputStream::read<bool>()
{
    return 0 != mIs.get();
}

template<>
Byte DataInputStream::read<Byte>()
{
    return static_cast<Byte>(mIs.get());
}

template<>
std::int8_t DataInputStream::read<std::int8_t>()
{
    auto const result{read<Byte>()};
    return static_cast<std::int8_t>(0x80 & result ? -1 - ~result : result);
}

template<>
std::uint16_t DataInputStream::read<std::uint16_t>()
{
    auto const result{static_cast<std::uint16_t>(read<Byte>() * 0x100)};
    return result + read<Byte>();
}

template<>
Short DataInputStream::read<Short>()
{
    auto const result{static_cast<Short>(read<std::int8_t>() * 0x100)};
    return result + read<Byte>();
}

template<>
std::uint32_t DataInputStream::read<std::uint32_t>()
{
    auto const result{
        static_cast<std::uint32_t>(read<std::uint16_t>() * 0x10000U)};
    return result + read<std::uint16_t>();
}

template<>
Integer DataInputStream::read<Integer>()
{
    auto const result{static_cast<Integer>(read<Short>() * 0x10000)};
    return result + read<std::uint16_t>();
}

template<>
Long DataInputStream::read<Long>()
{
    auto const result{static_cast<Long>(read<Integer>() * 0x100000000)};
    return result + read<std::uint32_t>();
}

template<>
std::string DataInputStream::read<std::string>()
{
    std::string result(static_cast<std::string::size_type>(read<Short>()), 0);
    mIs.read(result.data(), static_cast<std::streamsize>(result.size()));
    return result;
}

namespace
{

    constexpr float kExpBias{150.0f};
    constexpr std::uint32_t kMantShift{0};
    constexpr std::uint32_t kMantBits{23};
    constexpr std::uint32_t kExpShift{kMantShift + kMantBits};
    constexpr std::uint32_t kExpBits{8};
    constexpr std::uint32_t kSignShift{kExpShift + kExpBits};
    constexpr std::uint32_t kSignBits{1};

    constexpr std::uint32_t mask(std::uint32_t const bits) noexcept
    {
        return (1u << bits) - 1;
    }

} // namespace

// See
// https://docs.oracle.com/en/java/javase/15/docs/api/java.base/java/lang/Float.html#intBitsToFloat(int).
template<>
float DataInputStream::read<float>()
{
    std::uint32_t const intBits{read<std::uint32_t>()};
    float const s{(intBits >> kSignShift) & mask(kSignBits) ? -1.0f : 1.0f};
    std::uint32_t const e{(intBits >> kExpShift) & mask(kExpBits)};
    std::uint32_t const m0{(intBits >> kMantShift) & mask(kMantBits)};

    if (mask(kExpBits) == e) {
        return 0 == m0 ? s * std::numeric_limits<float>::infinity()
                       : std::numeric_limits<float>::quiet_NaN();
    }

    float const m{
        static_cast<float>(0 == e ? m0 << 1u : m0 | (1u << kMantBits))};

    return s * m * std::exp2f(static_cast<float>(e) - kExpBias);
}

} // namespace jois
