#include "libjois/ObjectInputStream.h"

#include "libjois/Array.h"
#include "libjois/DataInputStream.h"
#include "libjois/JavaValue.h"
#include "libjois/Object.h"
#include "libjois/ObjectStreamConstants.h"
#include "libjois/Registry.h"
#include "libjois/types.h"

#include <istream>
#include <stdexcept>
#include <string>
#include <variant>

// See
// https://docs.oracle.com/en/java/javase/14/docs/specs/serialization/protocol.html

namespace jois
{

ObjectInputStream::ObjectInputStream(std::istream& is)
: mIs{is}
, mRegistry{std::make_unique<Registry>()}
{
    is.exceptions(
        std::istream::badbit | std::istream::eofbit | std::istream::failbit);
    auto const magic{mIs.read<Short>()};
    if (ObjectStreamConstants::STREAM_MAGIC != magic) {
        throw std::runtime_error{"Invalid magic: " + std::to_string(magic)};
    }
    auto const version{mIs.read<Short>()};
    if (ObjectStreamConstants::STREAM_VERSION != version) {
        throw std::runtime_error{"Invalid version: " + std::to_string(version)};
    }
}

ObjectInputStream::ObjectInputStream(ObjectInputStream&&) noexcept = default;
ObjectInputStream::~ObjectInputStream() = default;

JavaValue ObjectInputStream::readObject()
{
    return std::visit(
        [](auto const& v)
        {
            return JavaValue{v};
        },
        mRegistry->readContent(mIs));
}

} // namespace jois
