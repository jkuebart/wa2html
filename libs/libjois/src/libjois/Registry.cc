#include "libjois/Registry.h"

#include "libjois/Array.h"
#include "libjois/Class.h"
#include "libjois/DataInputStream.h"
#include "libjois/Object.h"
#include "libjois/ObjectStreamConstants.h"

#include <cassert>
#include <stdexcept>
#include <string>
#include <utility>

namespace jois
{

bool Registry::isDefined(Entry const& entry)
{
    return std::visit(
        [](auto const& e)
        {
            return static_cast<bool>(e);
        },
        entry);
}

Integer Registry::insert(Entry&& value)
{
    mEntries.push_back(std::move(value));
    return static_cast<Integer>(mEntries.size() - 1) +
        ObjectStreamConstants::basicWireHandle;
}

void Registry::define(Integer const handle, Entry&& value)
{
    auto& entry{mEntries.at(index(handle))};

    if (isDefined(entry)) {
        throw std::runtime_error{
            "Handle already defined: " + std::to_string(handle)};
    }
    entry = std::move(value);
}

ObjectValue Registry::at(Integer const handle) const
{
    auto const& entry{mEntries.at(index(handle))};
    assert(isDefined(entry));
    return std::visit(
        [](auto const& e)
        {
            return ObjectValue{e.get()};
        },
        entry);
}

std::size_t Registry::index(Integer const handle)
{
    return static_cast<std::size_t>(
        handle - ObjectStreamConstants::basicWireHandle);
}

ObjectValue Registry::readContent(DataInputStream& is)
{
    auto const variant{is.read<Byte>()};
    switch (variant) {
    case ObjectStreamConstants::TC_NULL:
        return Null{};

    case ObjectStreamConstants::TC_REFERENCE:
        return at(is.read<Integer>());

    case ObjectStreamConstants::TC_CLASSDESC:
    {
        Integer const handle{insert(std::unique_ptr<ClassDesc>{})};
        define(handle, std::make_unique<ClassDesc>(is, *this));
        return at(handle);
    }

    case ObjectStreamConstants::TC_OBJECT:
    {
        auto const& clazz{read<Class>(is)};
        Integer const handle{insert(std::unique_ptr<ObjectDesc>{})};
        define(handle, std::make_unique<ObjectDesc>(is, *this, clazz));
        return at(handle);
    }

    case ObjectStreamConstants::TC_STRING:
    {
        Integer const handle{
            insert(std::make_unique<std::string>(is.read<std::string>()))};
        return at(handle);
    }

    case ObjectStreamConstants::TC_ARRAY:
    {
        auto const& clazz{read<Class>(is)};
        Integer const handle{insert(std::unique_ptr<ArrayDesc>{})};
        define(handle, std::make_unique<ArrayDesc>(is, *this, clazz));
        return at(handle);
    }

    default:
        throw std::runtime_error{"Unknown variant: " + std::to_string(variant)};
    }
}

namespace
{
    template<typename T>
    T readBlockdata(DataInputStream& is)
    {
        auto const blockdata{is.read<Byte>()};
        if (ObjectStreamConstants::TC_BLOCKDATA != blockdata) {
            throw std::runtime_error{
                "Invalid blockdata: " + std::to_string(blockdata)};
        }
        auto const length{is.read<Byte>()};
        if (sizeof(T) != length) {
            throw std::runtime_error{
                "Expected primitive of length " + std::to_string(sizeof(T)) +
                " but got " + std::to_string(length)};
        }
        return is.read<T>();
    }
} // namespace

template<>
Byte Registry::readPrimitive<Byte>(DataInputStream& is)
{
    return readBlockdata<Byte>(is);
}

template<>
Short Registry::readPrimitive<Short>(DataInputStream& is)
{
    return readBlockdata<Short>(is);
}

template<>
Integer Registry::readPrimitive<Integer>(DataInputStream& is)
{
    return readBlockdata<Integer>(is);
}

template<>
Long Registry::readPrimitive<Long>(DataInputStream& is)
{
    return readBlockdata<Long>(is);
}

} // namespace jois
