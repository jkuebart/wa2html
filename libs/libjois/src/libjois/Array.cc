#include "libjois/Array.h"

#include "libjois/Class.h"
#include "libjois/DataInputStream.h"

#include <ostream>
#include <stdexcept>
#include <string>

namespace jois
{

namespace
{

    template<typename T>
    std::vector<T> readPrimitiveArray(DataInputStream& is)
    {
        std::vector<T> result{};
        for (auto size{is.read<Integer>()}; 0 != size; --size) {
            result.push_back(is.read<T>());
        }
        return result;
    }

    ArrayDesc::ArrayValue
    readArrayValue(DataInputStream& is, Registry&, Class const& clazz)
    {
        if ("[B" == clazz->name) {
            return readPrimitiveArray<Byte>(is);
        }
        if ("[I" == clazz->name) {
            return readPrimitiveArray<Integer>(is);
        }
        if ("[J" == clazz->name) {
            return readPrimitiveArray<Long>(is);
        }
        if ("[S" == clazz->name) {
            return readPrimitiveArray<Short>(is);
        }
        throw std::runtime_error{"Unsupported array type " + clazz->name};
    }

} // namespace

ArrayDesc::ArrayDesc(
    DataInputStream& is,
    Registry& registry,
    Class const& aClass)
: clazz{aClass}
, value{readArrayValue(is, registry, aClass)}
{}

std::ostream& operator<<(std::ostream& os, Array const& array)
{
    if (!array) {
        return os << "null";
    }
    return os << "Array{class=" << array->clazz << " size="
              << std::visit(
                     [](auto const& a)
                     {
                         return a.size();
                     },
                     array->value)
              << '}';
}

} // namespace jois
