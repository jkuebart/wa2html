#include "libjois/types.h"

#include <iostream>

namespace jois
{

std::ostream& operator<<(std::ostream& os, Null)
{
    return os << "null";
}

} // namespace jois
