#ifndef LIBJOIS_ARRAY_H
#define LIBJOIS_ARRAY_H

#include "libjmeta/concat.h"
#include "libjmeta/transform.h"
#include "libjois/types.h"

#include <iosfwd>
#include <variant>
#include <vector>

namespace jois
{

class DataInputStream;
class Registry;

struct ArrayDesc
{
    using ArrayValue = jmeta::concat<
        jmeta::transform<std::vector, ObjectValue>,
        jmeta::transform<std::vector, PrimitiveValue>>;

    ArrayDesc(DataInputStream& is, Registry& registry, Class const& aClass);

    Class const clazz;
    ArrayValue const value;
};

std::ostream& operator<<(std::ostream& os, Array const& array);

} // namespace jois

#endif // LIBJOIS_ARRAY_H
