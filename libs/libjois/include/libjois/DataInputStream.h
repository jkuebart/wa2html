#ifndef LIBJOIS_DATAINPUTSTREAM_H
#define LIBJOIS_DATAINPUTSTREAM_H

#include "libjois/types.h"

#include <cstdint>
#include <iosfwd>
#include <string>

namespace jois
{

/// I provide input of basic Java data types compatible to DataInputStream.
class DataInputStream
{
public:
    /// @param is An input stream. The stream must be valid for the entire
    ///           lifetime of this object.
    ///
    /// @note The methods of this class signal failure using exceptions.
    ///       The underlying stream should also be configured for exceptions
    ///       by calling is.exceptions().
    explicit DataInputStream(std::istream& is) : mIs{is} {}

    template<typename T>
    T read();

private:
    std::istream& mIs;
};

template<>
bool DataInputStream::read<bool>();

template<>
Byte DataInputStream::read<Byte>();

template<>
std::int8_t DataInputStream::read<std::int8_t>();

template<>
std::uint16_t DataInputStream::read<std::uint16_t>();

template<>
Short DataInputStream::read<Short>();

template<>
std::uint32_t DataInputStream::read<std::uint32_t>();

template<>
Integer DataInputStream::read<Integer>();

template<>
Long DataInputStream::read<Long>();

template<>
std::string DataInputStream::read<std::string>();

template<>
float DataInputStream::read<float>();

} // namespace jois

#endif // LIBJOIS_DATAINPUTSTREAM_H
