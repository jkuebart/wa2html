#ifndef LIBJOIS_OBJECT_H
#define LIBJOIS_OBJECT_H

#include "libjois/JavaValue.h"
#include "libjois/types.h"

#include <iosfwd>
#include <string_view>
#include <vector>

namespace jois
{

class DataInputStream;
class Registry;

/// I represent a serialised object instance.
struct ObjectDesc
{
    ObjectDesc(DataInputStream& is, Registry& registry, Class const& aClass);

    /// @param name A field name.
    /// @return The value of the named field if this value represents an
    ///         object.
    /// @throws std::out_of_range if the named property doesn't exist.
    JavaValue const& at(std::string_view const& name) const;

    Class const clazz;
    std::vector<JavaValue> const values;
    std::vector<JavaValue> const annotation;
};

std::ostream& operator<<(std::ostream& os, Object const& object);

} // namespace jois

#endif // LIBJOIS_OBJECT_H
