#ifndef LIBJOIS_CLASS_H
#define LIBJOIS_CLASS_H

#include "libjois/types.h"

#include <cstddef>
#include <iosfwd>
#include <string>
#include <string_view>
#include <utility>
#include <vector>

namespace jois
{

class DataInputStream;
class Registry;

/// I represent information about one Java class.
struct ClassDesc
{
    using ClassId = std::pair<std::string_view, Long>;

    /// I represent information about one field of a Java class.
    struct FieldDesc
    {
        FieldDesc(DataInputStream& is, Registry& registry);

        Byte const typecode;
        std::string const name;
        std::string const type;
    };

    ClassDesc(DataInputStream& is, Registry& registry);

    /// @return The class name and serialVersionUID.
    ClassId classId() const;

    /// @return The number of fields in objects of this class, including
    ///         super classes.
    std::size_t numberOfFields() const;

    /// @param name The field name.
    /// @return The index of the named field for the most derived class
    ///         where it occurs.
    /// @throws std::out_of_range if there is no field of the given name.
    std::size_t fieldIndex(std::string_view const& name) const;

    std::string const name;
    Long const serialVersionUID;
    Byte const classDescFlags;
    std::vector<FieldDesc> const fields;
    Class const superClass;
};

std::ostream& operator<<(std::ostream& os, Class const& clazz);

} // namespace jois

#endif // LIBJOIS_CLASS_H
