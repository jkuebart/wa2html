#ifndef LIBJOIS_TYPES_H
#define LIBJOIS_TYPES_H

#include <cstdint>
#include <iosfwd>
#include <string>
#include <variant>

namespace jois
{

//
// Types which can represent the primitive Java types.
//

using Byte = std::uint8_t;
using Short = std::int16_t;
using Integer = std::int32_t;
using Long = std::int64_t;

using PrimitiveValue = std::variant<Byte, Short, Integer, Long, bool, float>;

//
// Types which can represent Java objects.
//

struct ArrayDesc;
struct ClassDesc;
struct ObjectDesc;

using Array = ArrayDesc const*;
using Class = ClassDesc const*;
using Object = ObjectDesc const*;
using String = std::string const*;

std::ostream& operator<<(std::ostream& os, String const& string);

//
// A type which represents a null pointer to a Java object of any type.
//

struct Null
{};

std::ostream& operator<<(std::ostream& os, Null);

using ObjectValue = std::variant<Array, Class, Object, String, Null>;

} // namespace jois

#endif // LIBJOIS_TYPES_H
