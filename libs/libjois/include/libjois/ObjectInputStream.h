#ifndef LIBJOIS_OBJECTINPUTSTREAM_H
#define LIBJOIS_OBJECTINPUTSTREAM_H

#include "libjois/DataInputStream.h"
#include "libjois/JavaValue.h"

#include <iosfwd>
#include <memory>

namespace jois
{

class Registry;

class ObjectInputStream
{
public:
    explicit ObjectInputStream(std::istream& is);

    ObjectInputStream(ObjectInputStream const&) = delete;
    ObjectInputStream(ObjectInputStream&&) noexcept;
    ObjectInputStream& operator=(ObjectInputStream const&) = delete;
    ObjectInputStream& operator=(ObjectInputStream&&) = delete;
    ~ObjectInputStream();

    /// @return An object read from the current position in the stream.
    JavaValue readObject();

private:
    DataInputStream mIs;
    std::unique_ptr<Registry> mRegistry;
};

} // namespace jois

#endif // LIBJOIS_OBJECTINPUTSTREAM_H
