#ifndef LIBJOIS_JAVAVALUE_H
#define LIBJOIS_JAVAVALUE_H

#include "libjmeta/concat.h"
#include "libjois/Class.h"
#include "libjois/types.h"

#include <iosfwd>
#include <string_view>
#include <type_traits>
#include <variant>

namespace jois
{

/// I represent any value that can be stored in an object's field.
class JavaValue
{
public:
    using Value = jmeta::concat<PrimitiveValue, ObjectValue>;

    template<typename T = Null>
    explicit JavaValue(
        T&& t = {},
        std::enable_if_t<std::is_convertible_v<T, Value>, int> = 0)
    : mValue{std::forward<T>(t)}
    {}

    /// @return If this value represents an array or an object, the object's
    ///         class.
    /// @throws std::bad_variant_access if this value isn't an array or an
    ///         object.
    Class getClass() const;

    /// @param name A field name.
    /// @return The value of the named field if this value represents an
    ///         object.
    /// @throws std::bad_variant_access if this value isn't an object.
    /// @throws std::out_of_range if the named property doesn't exist.
    JavaValue operator[](std::string_view const& name) const;

    /// @param index An array index.
    /// @return The value at the given index if this value represents an
    ///         array.
    /// @throws std::bad_variant_access if this value isn't an array.
    /// @throws std::out_of_range if the given index doesn't exist.
    JavaValue operator[](Integer index) const;

    template<typename T>
    friend auto get(JavaValue const& field)
    {
        return std::get<T>(field.mValue);
    }

    template<typename Func>
    friend auto visit(Func&& func, JavaValue const& field)
    {
        return std::visit(std::forward<Func>(func), field.mValue);
    }

private:
    Value mValue;
};

std::ostream& operator<<(std::ostream& os, JavaValue const& value);

} // namespace jois

#endif // LIBJOIS_JAVAVALUE_H
