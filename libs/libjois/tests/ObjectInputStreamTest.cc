#include "libjois/ObjectInputStream.h"

#include "libjois/JavaValue.h"

#include <cassert>
#include <exception>
#include <iostream>

namespace
{

int Main()
{
    jois::ObjectInputStream ois{std::cin};

    jois::JavaValue list1{ois.readObject()};
    std::cout << "list1: " << list1 << "\n  class: " << list1.getClass()
              << '\n';

    jois::JavaValue list2{ois.readObject()};
    std::cout << "list2: " << list2 << "\n  class: " << list2.getClass()
              << '\n';

    assert(get<jois::Object>(list1["next"]) == get<jois::Object>(list2));
    assert(get<jois::Object>(list2["next"]) == nullptr);

    return 0;
}

} // namespace

int main()
try {
    return Main();
} catch (std::exception& ex) {
    std::cerr << "Error: " << ex.what() << '\n';
    return 65; // EX_DATAERR
}
