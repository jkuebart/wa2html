#ifndef LIBJSQL_SIMPLEQUERY_H
#define LIBJSQL_SIMPLEQUERY_H

#include "libjsql/Query.h"
#include "libjsql/Statement.h"

#include <string_view>

namespace jsql
{

class Db;

/// I provide a convenient way to perform an SQL statement once.
class SimpleQuery
{
public:
    using Arguments = Query::Arguments;

    /// @param db The database connection.
    /// @param sql A single SQL statement.
    /// @param args Arguments to bind to the statement.
    SimpleQuery(
        Db const& db,
        std::string_view const& sql,
        Arguments const& args = {});

    /// @return Whether this query returns another row.
    bool empty() const;

    /// @return The current row.
    Query const& front() const;

    /// Move to the next row in the result.
    void popFront();

private:
    Statement mStatement;
    Query mQuery;
};

} // namespace jsql

#endif // LIBJSQL_SIMPLEQUERY_H
