#ifndef LIBJSQL_STATEMENT_H
#define LIBJSQL_STATEMENT_H

#include <cstddef>
#include <memory>
#include <string>
#include <string_view>
#include <unordered_map>

struct sqlite3;
struct sqlite3_stmt;

namespace jsql
{

class Db;

/// I represent a prepared SQL statement.
///
/// A Query object can bind arguments to the parameters (if any) and perform
/// the statement.
///
/// @note Only *one* Query object can be created for a Statement at
///       any one time. Attempting to create a Query while another one is
///       still in scope for the same Statement will raise an exception.
class Statement
{
public:
    using size_type = std::size_t;
    friend class Query;

    /// @param db The database connection.
    /// @param sql A single SQL statement.
    Statement(Db const& db, std::string_view const& sql);

    /// @return The underlying statement handle or nullptr if there is
    ///         currently a Query for this Statement.
    sqlite3_stmt* get() const noexcept;

    /// @return The underlying database handle.
    /// @throws std::runtime_error if there is currently a Query for this
    ///         Statement.
    sqlite3* db() const;

private:
    int checkColumn(size_type column) const;

private:
    std::unique_ptr<sqlite3_stmt, int (*)(sqlite3_stmt*)> mStatement;
    size_type const mSize;
    mutable std::unordered_map<std::string, size_type> mColumnNames;
};

} // namespace jsql

#endif // LIBJSQL_STATEMENT_H
