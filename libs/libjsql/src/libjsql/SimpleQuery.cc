#include "libjsql/SimpleQuery.h"

namespace jsql
{

SimpleQuery::SimpleQuery(
    Db const& db,
    std::string_view const& sql,
    Arguments const& args)
: mStatement{db, sql}
, mQuery{mStatement, args}
{}

bool SimpleQuery::empty() const
{
    return mQuery.empty();
}

Query const& SimpleQuery::front() const
{
    return mQuery.front();
}

void SimpleQuery::popFront()
{
    return mQuery.popFront();
}

} // namespace jsql
