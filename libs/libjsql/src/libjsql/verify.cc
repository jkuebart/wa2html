#include "libjsql/verify.h"

#include <sqlite3.h>

#include <stdexcept>
#include <string>

namespace jsql
{

int verify(
    int const result,
    std::string_view const& topic,
    std::string_view const& detail)
{
    if (SQLITE_DONE != result && SQLITE_OK != result && SQLITE_ROW != result) {
        throw std::runtime_error{
            std::string{topic} + (topic.empty() ? "" : ": ") +
            sqlite3_errstr(result) + (detail.empty() ? "" : ": ") +
            std::string{detail}};
    }
    return result;
}

} // namespace jsql
