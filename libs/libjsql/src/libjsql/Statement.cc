#include "libjsql/Statement.h"

#include "libjsql/Db.h"

#include <sqlite3.h>

#include <stdexcept>

namespace jsql
{

Statement::Statement(Db const& db, std::string_view const& sql)
: mStatement{
    [&db, &sql]() {
        sqlite3_stmt* stmt{nullptr};
        db.verify(sqlite3_prepare_v2(
            db.get(),
            sql.data(),
            static_cast<int>(sql.size()),
            &stmt,
            nullptr));
        return stmt;
    }(),
    sqlite3_finalize}
, mSize{static_cast<size_type>(sqlite3_column_count(mStatement.get()))}
, mColumnNames{}
{}

int Statement::checkColumn(size_type const column) const
{
    if (mSize <= column) {
        throw std::out_of_range(
            "Column index " + std::to_string(column) +
            " is not between 0 and " + std::to_string(mSize));
    }
    return static_cast<int>(column);
}

sqlite3_stmt* Statement::get() const noexcept
{
    return mStatement.get();
}

sqlite3* Statement::db() const
{
    if (!mStatement) {
        throw std::runtime_error{
            "Can't return database handle while there is a Query."};
    }
    return sqlite3_db_handle(get());
}

} // namespace jsql
