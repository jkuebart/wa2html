#include "libjhtml/Document.h"
#include "libjmsg/Chat.h"
#include "libjmsg/ChatIndex.h"
#include "libjsql/Db.h"
#include "libjsql/SimpleQuery.h"
#include "libjwa/Chat.h"
#include "libjwa/Chats.h"

#include <algorithm>
#include <exception>
#include <fstream>
#include <iostream>
#include <optional>
#include <string>
#include <string_view>
#include <unordered_map>
#include <vector>

namespace
{

int Main(std::vector<std::string_view> args)
{
    if (args.size() < 2 || 3 < args.size()) {
        std::cerr << "usage: " << args[0] << R"( msgstore [wa]

    Create a HTML archive of the conversations in the msgstore database,
    with the contacts found in the wa database.
)";
        return 64; // EX_USAGE
    }

    std::unordered_map<std::string, std::string> jids;
    if (2 < args.size()) {
        jsql::Db wa{std::string{args[2]}};
        jsql::SimpleQuery wa_contacts{
            wa,
            R"(
                select
                    jid,
                    coalesce(display_name, wa_name) as display_name
                from wa_contacts;
            )"};
        for (; !wa_contacts.empty(); wa_contacts.popFront()) {
            auto const& row{wa_contacts.front()};
            if (auto const display_name{row["display_name"]}) {
                jids.emplace(row["jid"].value(), *display_name);
            }
        }
    }

    jsql::Db const msgstore{std::string{args[1]}};
    std::optional<jsql::Db> const wa{
        2 < args.size() ? std::optional<jsql::Db>{std::string{args[2]}}
                        : std::nullopt};
    jwa::Chats chats{wa ? jwa::Chats{msgstore, *wa} : jwa::Chats{msgstore}};
    std::ofstream index{"index.html"};
    jhtml::Document indexDoc{index};
    jmsg::ChatIndex chatIndex{indexDoc};
    for (; !chats.empty(); chats.popFront()) {
        jwa::Chat chat{chats.front()};
        chatIndex << chat;

        std::string const jid{chat.row()["raw_string_jid"].value()};
        std::ofstream stream{chat.filename()};
        jhtml::Document doc{stream};
        jmsg::Chat htmlChat{
            doc,
            jid,
            chat.displayName(),
            chat.groupParticipants()};
        for (; !chat.empty(); chat.popFront()) {
            htmlChat << chat.front();
#ifdef notdef
            if (auto const filePath{row["file_path"]}) {
                std::cout << *filePath << '\n';
            }
#endif
        }
    }

    return 0;
}

} // namespace

int main(int argc, char* argv[])
try {
    return Main(std::vector<std::string_view>{argv, argc + argv});
} catch (std::exception& exc) {
    std::cerr << "Error: " << exc.what() << "\n";
    return 1;
}
