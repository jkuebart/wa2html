#include "libjmsg/htmlHead.h"

#include "libjhtml/Element.h"
#include "libjhtml/Link.h"
#include "libjhtml/Meta.h"

namespace jmsg
{

jhtml::Document& htmlHead(jhtml::Document& doc, std::string_view const& title)
{
    jhtml::Element{doc, "meta", {{"charset", "utf-8"}}};
    jhtml::Meta{doc, "viewport", "width=device-width, initial-scale=1"};
    jhtml::Link{doc, "stylesheet", "wa2html.css"};
    jhtml::Element{doc, "title", {}, title};
    return doc;
}

} // namespace jmsg
