#include "libjmsg/css.h"

#include "libjhtml/Element.h"
#include "libjhtml/Uri.h"

#include <string>

namespace
{

constexpr std::string_view kGroupSvg{"group.svg"};
constexpr std::string_view kUserSvg{"user.svg"};

jhtml::Document& avatar(
    jhtml::Document& doc,
    std::string_view const& jid,
    std::string_view const& src)
{
    jhtml::Element object{
        doc,
        "object",
        {{"class", css::kAvatar},
         {"data", jhtml::Uri{"Avatars/" + std::string{jid} + ".j"}},
         {"type", "image/jpeg"}}};
    jhtml::Element{doc, "img", {{"alt", jid}, {"src", jhtml::Uri{src}}}};
    return doc;
}

} // namespace

namespace css
{

jhtml::Document& groupAvatar(jhtml::Document& doc, std::string_view const& jid)
{
    return avatar(doc, jid, kGroupSvg);
}

jhtml::Document& userAvatar(jhtml::Document& doc, std::string_view const& jid)
{
    return avatar(doc, jid, kUserSvg);
}

} // namespace css
