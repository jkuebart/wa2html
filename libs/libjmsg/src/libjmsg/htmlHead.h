#ifndef LIBJMSG_HTMLHEAD_H
#define LIBJMSG_HTMLHEAD_H

#include <string_view>

namespace jhtml
{

class Document;

}

namespace jmsg
{

jhtml::Document& htmlHead(jhtml::Document& doc, std::string_view const& title);

}

#endif // LIBJMSG_HTMLHEAD_H
