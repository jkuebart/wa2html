#include "libjmsg/Message.h"

#include "libjhtml/Document.h"
#include "libjhtml/Time.h"
#include "libjhtml/Uri.h"
#include "libjmsg/Chat.h"
#include "libjmsg/css.h"

#include <chrono>
#include <sstream>
#include <string>

namespace jmsg
{

namespace
{

    std::string classForStatus(
        Message::Status const status,
        bool const forwarded)
    {
        std::ostringstream result;

        switch (status) {
        case Message::Status::kDelivered:
            result << css::kOutbound << ' ' << css::kDelivered;
            break;
        case Message::Status::kNotificationBiz:
            result << css::kNotificationBiz;
            break;
        case Message::Status::kNotificationE2e:
            result << css::kNotificationE2e;
            break;
        case Message::Status::kReceived:
            result << css::kInbound;
            break;
        case Message::Status::kSeen:
            result << css::kOutbound << ' ' << css::kSeen;
            break;
        case Message::Status::kSent:
            result << css::kOutbound << ' ' << css::kSent;
            break;
        case Message::Status::kSystem:
            result << css::kSystem;
            break;
        }

        if (forwarded) {
            result << ' ' << css::kForwarded;
        }

        return result.str();
    }

} // namespace

Message::Para::Para(Message& message)
: mDoc{message.mChat.doc()}
, mPara{mDoc, "p"}
{}

Message::Para& Message::Para::text(std::string_view const& text)
{
    mDoc.text(text);
    return *this;
}

Message::Quote::Quote(Message& message)
: mBlockquote{message.mChat.doc(), "blockquote"}
{}

Message::Figure::Figure(Message& message)
: mChat{message.mChat}
, mFigure{message.mChat.doc(), "figure"}
{}

Chat& Message::Figure::chat() const
{
    return mChat;
}

Message::Figure& Message::Figure::caption(std::string_view const& text)
{
    Caption{*this, text};
    return *this;
}

Message::Caption::Caption(Figure& figure, std::string_view const& text)
: mCaption{figure.chat().doc(), "figcaption", {}, text}
{}

Message::Message(
    Chat& chat,
    int const id,
    Status const status,
    bool const forwarded)
: mChat{chat}
, mSystem{Status::kSystem <= status}
, mDiv{
      mChat.doc(),
      "div",
      {{"class", classForStatus(status, forwarded)},
       {"id", std::to_string(id)}}}
{}

Message::~Message()
{
    if (!mSystem) {
        jhtml::Time{mChat.doc(), "%H:%M", mChat.timestamp()};
    }
}

Message& Message::author(std::string_view const& author)
{
    jhtml::Element{mChat.doc(), "div", {{"class", css::kSender}}, author};
    return *this;
}

Message& Message::note(std::string_view const& text)
{
    jhtml::Element{mChat.doc(), "p", {{"class", css::kNote}}, text};
    return *this;
}

Message& Message::text(std::string_view const& text)
{
    jhtml::Element{mChat.doc(), "p", {}, text};
    return *this;
}

Message& Message::thumbnail(jhtml::DataUri const& src)
{
    jhtml::Element{mChat.doc(), "img", {{"src", src}}};
    return *this;
}

Message& Message::thumbnail(
    std::string_view const& alt,
    jhtml::DataUri const& src)
{
    jhtml::Element{mChat.doc(), "img", {{"alt", alt}, {"src", src}}};
    return *this;
}

Message& Message::sticker(std::string_view const& fileName)
{
    jhtml::Element{
        mChat.doc(),
        "img",
        {{"alt", fileName}, {"class", "sticker"}, {"src", fileName}}};
    return *this;
}

Message& Message::image(
    std::string_view const& mimeType,
    std::optional<std::string_view> const& fileName,
    jhtml::DataUri const& fallbackUri)
{
    // An optional surrounding <object> if there is a file name.
    std::optional<jhtml::Element> object{};
    if (fileName) {
        object.emplace(
            mChat.doc(),
            "object",
            jhtml::Attributes{
                {"data", jhtml::Uri{*fileName}},
                {"type", mimeType}});
    }

    // A nested <img> as a fallback.
    return thumbnail(fileName.value_or("[Not downloaded]"), fallbackUri);
}

Message& Message::video(
    std::optional<std::string_view> const& fileName,
    jhtml::DataUri const& poster)
{
    if (fileName) {
        jhtml::Element{
            mChat.doc(),
            "video",
            {{"controls", "controls"},
             {"poster", poster},
             {"src", jhtml::Uri{*fileName}}}};
    } else {
        thumbnail("[Not downloaded]", poster);
    }
    return *this;
}

Message& Message::audio(std::string_view const& fileName)
{
    jhtml::Element{
        mChat.doc(),
        "audio",
        {{"controls", "controls"}, {"src", jhtml::Uri{fileName}}}};
    return *this;
}

Message& Message::link(jhtml::Uri const& href, std::string_view const& text)
{
    jhtml::Element p{mChat.doc(), "p"};
    jhtml::Element{mChat.doc(), "a", {{"href", href}}, text};
    return *this;
}

Message& Message::dataUri(
    jhtml::DataUri const& dataUri,
    std::string_view const& text)
{
    jhtml::Element p{mChat.doc(), "p"};
    jhtml::Element{mChat.doc(), "a", {{"href", dataUri}}, text};
    return *this;
}

} // namespace jmsg
