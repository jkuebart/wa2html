#ifndef LIBJMSG_CSS_H
#define LIBJMSG_CSS_H

#include <string_view>

namespace jhtml
{

class Document;

}

namespace css
{

constexpr std::string_view kAvatar{"avatar"};
constexpr std::string_view kConversation{"conversation"};
constexpr std::string_view kDelivered{"delivered"};
constexpr std::string_view kForwarded{"forwarded"};
constexpr std::string_view kInbound{"inbound"};
constexpr std::string_view kNote{"note"};
constexpr std::string_view kNotificationBiz{"notification-biz"};
constexpr std::string_view kNotificationE2e{"notification-e2e"};
constexpr std::string_view kOutbound{"outbound"};
constexpr std::string_view kSeen{"seen"};
constexpr std::string_view kSender{"sender"};
constexpr std::string_view kSent{"sent"};
constexpr std::string_view kSystem{"system"};
constexpr std::string_view kUnseen{"unseen"};

jhtml::Document& groupAvatar(jhtml::Document& doc, std::string_view const& jid);
jhtml::Document& userAvatar(jhtml::Document& doc, std::string_view const& jid);

} // namespace css

#endif // LIBJMSG_CSS_H
