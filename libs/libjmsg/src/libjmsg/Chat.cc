#include "libjmsg/Chat.h"

#include "libjhtml/DataUri.h"
#include "libjhtml/Document.h"
#include "libjhtml/Time.h"
#include "libjmsg/Message.h"
#include "libjmsg/css.h"
#include "libjmsg/htmlHead.h"

#include <date/date.h>

#include <algorithm>
#include <iostream>
#include <iterator>
#include <string>
#include <version>

using date::local_days;
using date::year;
using std::chrono::system_clock;

#if defined(__cpp_lib_chrono) && 201907L <= __cpp_lib_chrono

using std::chrono::days;

namespace
{

local_days localDays(system_clock::time_point const& timestamp)
{
    return floor<days>(std::chrono::current_zone()->to_local(timestamp));
}

} // namespace

#else

#include <ctime>

namespace
{

local_days localDays(system_clock::time_point const& timestamp)
{
    std::time_t const t{system_clock::to_time_t(timestamp)};
    std::tm const* const tm{std::localtime(&t)};
    return local_days{
        year{1900 + tm->tm_year} / (1 + tm->tm_mon) / tm->tm_mday};
}

} // namespace

#endif

namespace
{

jhtml::Document& htmlHeader(
    jhtml::Document& doc,
    std::string_view const& jid,
    std::string_view const& displayName,
    std::vector<std::string> const& participants)
{
    jhtml::Element header{doc, "header"};

    if (participants.empty()) {
        css::userAvatar(doc, jid);
    } else {
        css::groupAvatar(doc, jid);
    }

    jhtml::Element div{doc, "div"};
    jhtml::Element{doc, "p", {}, displayName};
    if (!participants.empty()) {
        jhtml::Element ul{doc, "ul"};
        for (auto const& participant : participants) {
            jhtml::Element{doc, "li", {}, participant};
        }
    }

    return doc;
}

} // namespace

namespace jmsg
{

Chat::Chat(
    jhtml::Document& doc,
    std::string_view const& jid,
    std::string_view const& displayName,
    std::vector<std::string> const& participants)
: mDoc{doc}
, mBody{htmlHead(mDoc, displayName), "body", {{"class", css::kConversation}}}
, mMain{htmlHeader(mDoc, jid, displayName, participants), "main"}
, mTimestamp{}
{}

jhtml::Document& Chat::doc()
{
    return mDoc;
}

Chat& Chat::timestamp(system_clock::time_point const& timestamp)
{
    if (!mTimestamp || localDays(*mTimestamp) != localDays(timestamp)) {
        jhtml::Element div{mDoc, "div", {{"class", css::kSystem}}};
        jhtml::Time{mDoc, "%d/%m/%Y", timestamp};
    }
    mTimestamp = timestamp;
    return *this;
}

system_clock::time_point const& Chat::timestamp() const
{
    return mTimestamp.value();
}

} // namespace jmsg
