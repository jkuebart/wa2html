#include "libjmsg/ChatIndex.h"

#include "libjhtml/Time.h"
#include "libjmsg/css.h"
#include "libjmsg/htmlHead.h"

#include <string>
#include <utility>

using std::chrono::system_clock;

namespace
{

constexpr std::string_view kContext{"conversation"};

} // namespace

namespace jmsg
{

ChatIndex::ChatIndex(jhtml::Document& doc)
: mDoc{doc}
, mMenu{std::in_place, htmlHead(mDoc, "WhatsApp HTML Archive"), "menu"}
{}

ChatIndex::~ChatIndex()
{
    mMenu.reset();
    jhtml::Element{mDoc, "iframe", {{"name", kContext}}};
}

ChatIndex& ChatIndex::chat(
    std::string_view const& jid,
    std::string_view const& filename,
    std::string_view const& displayName,
    std::optional<system_clock::time_point> const& timestamp,
    std::string_view const& displayMessage,
    unsigned const unseen,
    std::optional<int> const unreadMessageId,
    Type const type)
{
    jhtml::Element li{mDoc, "li"};
    {
        jhtml::Element a{mDoc, "a", {{"href", filename}, {"target", kContext}}};

        if (Type::kUser == type) {
            css::userAvatar(mDoc, jid);
        } else {
            css::groupAvatar(mDoc, jid);
        }
    }

    {
        std::string const id{
            unreadMessageId ? "#" + std::to_string(*unreadMessageId) : ""};
        jhtml::Element a{
            mDoc,
            "a",
            {{"href", std::string{filename} + id}, {"target", kContext}}};

        {
            jhtml::Element p{mDoc, "p"};
            jhtml::Element{mDoc, "span", {}, displayName};
            if (timestamp) {
                jhtml::Time{mDoc, "%d/%m/%Y", *timestamp};
            }
        }

        {
            jhtml::Element p{mDoc, "p"};
            jhtml::Element{mDoc, "span", {}, displayMessage};
            if (0 != unseen) {
                jhtml::Element{
                    mDoc,
                    "span",
                    {{"class", css::kUnseen}},
                    std::to_string(unseen)};
            }
        }
    }

    return *this;
}

} // namespace jmsg
