#ifndef LIBJMSG_CHATINDEX_H
#define LIBJMSG_CHATINDEX_H

#include "libjhtml/Element.h"

#include <chrono>
#include <optional>
#include <string_view>

namespace jhtml
{

class Document;

}

namespace jmsg
{

class ChatIndex
{
public:
    enum class Type
    {
        kGroup,
        kUser
    };

    ChatIndex(jhtml::Document& doc);
    ~ChatIndex();

    ChatIndex& chat(
        std::string_view const& jid,
        std::string_view const& filename,
        std::string_view const& displayName,
        std::optional<std::chrono::system_clock::time_point> const& timestamp,
        std::string_view const& displayMessage,
        unsigned unseen,
        std::optional<int> unreadMessageId,
        Type type);

private:
    jhtml::Document& mDoc;
    std::optional<jhtml::Element> mMenu;
};

} // namespace jmsg

#endif // LIBJMSG_CHATINDEX_H
