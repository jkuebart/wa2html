#ifndef LIBJMSG_MESSAGE_H
#define LIBJMSG_MESSAGE_H

#include "libjhtml/Element.h"

#include <optional>
#include <string_view>

namespace jhtml
{

struct Uri;
struct DataUri;

} // namespace jhtml

namespace jmsg
{

class Chat;

class Message
{
public:
    /// Wrap contents in a paragraph.
    class Para
    {
    public:
        explicit Para(Message& message);

        Para& text(std::string_view const& text);

    private:
        jhtml::Document& mDoc;
        jhtml::Element mPara;
    };

    /// Wrap contents in a quote.
    class Quote
    {
    public:
        explicit Quote(Message& message);

    private:
        jhtml::Element mBlockquote;
    };

    /// Wrap contents in a figure.
    class Figure
    {
    public:
        explicit Figure(Message& message);

        Chat& chat() const;
        Figure& caption(std::string_view const& text);

    private:
        Chat& mChat;
        jhtml::Element mFigure;
    };

    /// Add a caption to a figure.
    class Caption
    {
    public:
        explicit Caption(Figure& figure, std::string_view const& text = "");

    private:
        jhtml::Element mCaption;
    };

    enum class Status
    {
        /// An outbound message that was sent and delivered.
        kDelivered,

        /// An inbound message.
        kReceived,

        /// An outbound message that was sent, delivered and seen.
        kSeen,

        /// An outbound message that was sent.
        kSent,

        /// A general system message.
        kSystem,

        /// A system message related to a business chat.
        kNotificationBiz,

        /// A system message related to end to end encryption.
        kNotificationE2e
    };

    Message(Chat& chat, int id, Status status, bool forwarded);

    ~Message();

    Message& author(std::string_view const& author);
    Message& note(std::string_view const& text);
    Message& text(std::string_view const& text);
    Message& thumbnail(jhtml::DataUri const& src);
    Message& thumbnail(std::string_view const& alt, jhtml::DataUri const& src);
    Message& sticker(std::string_view const& fileName);
    Message& image(
        std::string_view const& mimeType,
        std::optional<std::string_view> const& fileName,
        jhtml::DataUri const& fallbackUri);
    Message& video(
        std::optional<std::string_view> const& fileName,
        jhtml::DataUri const& poster);
    Message& audio(std::string_view const& fileName);
    Message& link(jhtml::Uri const& href, std::string_view const& text);
    Message& dataUri(
        jhtml::DataUri const& dataUri,
        std::string_view const& text);

private:
    Chat& mChat;
    bool const mSystem;
    jhtml::Element mDiv;
};

} // namespace jmsg

#endif // LIBJMSG_MESSAGE_H
