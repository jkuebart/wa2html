#ifndef LIBJMSG_CHAT_H
#define LIBJMSG_CHAT_H

#include "libjhtml/Element.h"

#include <chrono>
#include <optional>
#include <string>
#include <string_view>
#include <vector>

namespace jhtml
{

class Document;

}

namespace jmsg
{

class Chat
{
public:
    Chat(
        jhtml::Document& doc,
        std::string_view const& jid,
        std::string_view const& displayName,
        std::vector<std::string> const& participants);

    jhtml::Document& doc();

    Chat& timestamp(std::chrono::system_clock::time_point const& timestamp);
    std::chrono::system_clock::time_point const& timestamp() const;

private:
    jhtml::Document& mDoc;
    jhtml::Element mBody;
    jhtml::Element mMain;
    std::optional<std::chrono::system_clock::time_point> mTimestamp;
};

} // namespace jmsg

#endif // LIBJMSG_CHAT_H
