#ifndef LIBJWA_MESSAGE_H
#define LIBJWA_MESSAGE_H

#include "libjhtml/DataUri.h"

#include <optional>
#include <string>
#include <string_view>
#include <unordered_map>
#include <vector>

namespace jmsg
{

class Chat;
class Message;

} // namespace jmsg

namespace jsql
{

class Query;
class Statement;

} // namespace jsql

namespace jwa
{

class Chat;

class Message
{
public:
    Message(Chat const& chat, jsql::Query const& row, jsql::Statement& quotes);

    friend jmsg::Chat& operator<<(jmsg::Chat& chat, Message const& message);

private:
    Message const& systemMessage(jmsg::Chat& chat) const;
    Message const& textMessage(jmsg::Chat& chat) const;
    Message const& messageText(jmsg::Message& message) const;

private:
    Chat const& mChat;
    jsql::Query const& mRow;
    jsql::Statement& mQuotes;
    int const mId;
    std::string const mRemoteJid;
    bool const mFromMe;
    std::optional<std::string> const mData;
    std::optional<std::string> const mMimeType;
    int const mType;
    int const mMediaSize;
    std::optional<std::string> const mMediaUrl;
    std::optional<std::string> const mMediaName;
    std::optional<int> const mMediaDuration;
    std::optional<double> const mLatitude;
    std::optional<double> const mLongitude;
    std::optional<std::string> const mThumbImage;
    std::optional<std::string> const mRemoteResource;
    std::optional<std::string> const mMediaCaption;
    std::optional<jhtml::DataUri> const mThumbnail;
    std::optional<std::string> const mFilePath;
};

} // namespace jwa

#endif // LIBJWA_MESSAGE_H
