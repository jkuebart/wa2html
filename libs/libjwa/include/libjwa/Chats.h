#ifndef LIBJWA_CHATS_H
#define LIBJWA_CHATS_H

#include "libjsql/SimpleQuery.h"

#include <string>
#include <unordered_map>

namespace jmsg
{

class ChatIndex;

}

namespace jwa
{

class Chat;

/// A range of chats stored in a WhatsApp database.
class Chats
{
public:
    /// @param msgstore The message database.
    explicit Chats(jsql::Db const& msgstore);

    /// @param msgstore The message database.
    /// @param wa The contacts database.
    Chats(jsql::Db const& msgstore, jsql::Db const& wa);

    bool empty() const;
    Chat front();
    void popFront();

private:
    jsql::SimpleQuery mChats;
    jsql::Statement mMessages;
    jsql::Statement mQuotes;
    jsql::Statement mParticipants;
    std::unordered_map<std::string, std::string> mJids;
};

} // namespace jwa

#endif // LIBJWA_CHATS_H
