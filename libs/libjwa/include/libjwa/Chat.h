#ifndef LIBJWA_CHAT_H
#define LIBJWA_CHAT_H

#include "libjsql/Query.h"
#include "libjwa/Message.h"

#include <unordered_map>

namespace jmsg
{

class ChatIndex;

}

namespace jwa
{

/// A range of messages in a chat.
class Chat
{
public:
    static std::string formatJid(std::string_view const& jid);

    Chat(
        std::unordered_map<std::string, std::string> const& jids,
        jsql::Query const& row,
        jsql::Statement& messages,
        jsql::Statement& quotes,
        jsql::Statement& groupParticipants);

    std::string displayName() const;
    std::string filename() const;

    bool empty() const;
    Message front() const;
    void popFront();

    std::string const& jid() const noexcept;
    std::string lookup(std::string const& jid) const;
    jsql::Query const& row() const noexcept;
    std::vector<std::string> groupParticipants() const;

private:
    std::unordered_map<std::string, std::string> const& mJids;
    jsql::Query const& mRow;
    std::string const mJid;
    jsql::Query mMessages;
    jsql::Statement& mQuotes;
    jsql::Statement& mParticipants;
};

jmsg::ChatIndex& operator<<(jmsg::ChatIndex& chatIndex, Chat const& chat);

} // namespace jwa

#endif // LIBJWA_CHAT_H
