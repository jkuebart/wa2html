#include "libjwa/Message.h"

#include "date/date.h"
#include "libjhtml/DataUri.h"
#include "libjhtml/Uri.h"
#include "libjmsg/Chat.h"
#include "libjmsg/Message.h"
#include "libjois/Array.h"
#include "libjois/Class.h"
#include "libjois/JavaValue.h"
#include "libjois/Object.h"
#include "libjois/ObjectInputStream.h"
#include "libjois/types.h"
#include "libjsql/Query.h"
#include "libjwa/Chat.h"

#include <cassert>
#include <chrono>
#include <iostream>
#include <sstream>

using date::sys_days;
using date::year;
using date::year_month_day;
using std::chrono::milliseconds;
using std::chrono::system_clock;

namespace
{

/// WhatsApp time stamps are relative to this date.
constexpr sys_days kWhatsAppEpoch{year_month_day{year{1970} / 1 / 1}};

namespace WaType
{

    constexpr int kText{0};
    constexpr int kImage{1};
    constexpr int kAudio{2};
    constexpr int kVideo{3};
    constexpr int kVCard{4};
    constexpr int kLocation{5};
    constexpr int kFile{9};
    constexpr int kCallMissed{10};
    constexpr int kGif{13};
    constexpr int kDeleted{15};
    constexpr int kLocationLive{16};
    constexpr int kSticker{20};
    constexpr int kGroupInvite{24};

} // namespace WaType

namespace Status
{

    constexpr int kSent{0x4};
    constexpr int kDelivered{kSent | 0x1};
#ifndef NDEBUG
    constexpr int kSentAudio{0x8};
#endif
    constexpr int kSeen{kDelivered | 0x8};

    constexpr int kSystem{0x6};

} // namespace Status

namespace System
{

    constexpr int kCallMissed{0};
    constexpr int kSubjectChanged{1};
    constexpr int kUnknown0{2};
    constexpr int kGroupAddedYou{4};
    constexpr int kGroupLeft{5};
    constexpr int kIconChanged{6};
    constexpr int kNumberChanged0{10};
    constexpr int kGroupCreated{11};
    constexpr int kGroupAdded{12};
    constexpr int kGroupLeftMulti{13};
    constexpr int kGroupRemovedMulti{14};
    constexpr int kGroupAdminGranted{15};
    constexpr int kE2eChanged{18};
    constexpr int kGroupE2e{19};
    constexpr int kGroupJoined{20};
    constexpr int kBizAccount0{22};
    constexpr int kGroupDescription{27};
    constexpr int kNumberChanged1{28};
    constexpr int kGroupInfoRestricted{29};
    constexpr int kGroupInfoUnrestricted{30};
    constexpr int kGroupRestricted{31};
    constexpr int kGroupUnrestricted{32};
    constexpr int kBizAccount1{46};
    constexpr int kBizOfficial{47};
    constexpr int kBizToStandard{50};

} // namespace System

/// The following classes are obfuscated versions of each other. They share
/// the layout of one integer (the new photo id) followed by two byte
/// arrays (the new and the old photo, respectively). That's why we can use
/// the same code to process all of them.
constexpr jois::ClassDesc::ClassId kProfilePhotoChangeClassIds[]{
    {"com.whatsapp.a0b", -1},
    {"com.whatsapp.a5t", -1},
    {"com.whatsapp.c.ba", 3790253565779712530L},
    {"com.whatsapp.data.ProfilePhotoChange", -1}};
constexpr unsigned kNewPhotoIndex{1};
constexpr unsigned kOldPhotoIndex{2};

struct JavaObject
{
    /// Read a JavaValue from the given string.
    ///
    /// @note The lifetime of the JavaValue is constrained by the lifetime
    ///       of the returned instance.
    static std::optional<JavaObject> read(
        std::optional<std::string_view> const& string)
    {
        if (!string) {
            return std::nullopt;
        }
        return JavaObject{*string};
    }

    explicit JavaObject(std::string_view const& string)
    : inputStream{std::string{string}}
    , objectInputStream{inputStream}
    , object{objectInputStream.readObject()}
    {}

    std::string toString() const
    {
        std::ostringstream str{};
        str << object;
        return str.str();
    }

    /// @param chat The chat for which to resolve names.
    /// @return The formatted list of people.
    ///
    /// @note This method only works if this Java object contains an
    ///       ArrayList with JIDs.
    std::string asPeople(jwa::Chat const& chat) const
    {
        std::ostringstream result{};
        auto const list{get<jois::Object>(object)};
        char const* sep{""};
        for (auto const& person : list->annotation) {
            result << sep << chat.lookup(*get<jois::String>(person));
            sep = ", ";
        }
        return result.str();
    }

    std::istringstream inputStream;
    jois::ObjectInputStream objectInputStream;
    jois::JavaValue object;
};

void makeImageTag(jmsg::Message& message, jois::JavaValue const& value)
{
    if (auto const& array{get<jois::Array>(value)}) {
        auto const& data{std::get<std::vector<jois::Byte>>(array->value)};
        message.thumbnail(jhtml::DataUri{"image/jpeg", data});
    }
}

} // namespace

namespace jwa
{

Message::Message(
    Chat const& chat,
    jsql::Query const& row,
    jsql::Statement& quotes)
: mChat{chat}
, mRow{row}
, mQuotes{quotes}
, mId{row.at<int>("_id").value()}
, mRemoteJid{row["key_remote_jid"].value()}
, mFromMe{1 == row.at<int>("key_from_me").value()}
, mData{row["data"]}
, mMimeType{row["media_mime_type"]}
, mType{row.at<int>("media_wa_type").value()}
, mMediaSize{row.at<int>("media_size").value()}
, mMediaUrl{row["media_url"]}
, mMediaName{row["media_name"]}
, mMediaDuration{row.at<int>("media_duration")}
, mLatitude{row.at<double>("latitude")}
, mLongitude{row.at<double>("longitude")}
, mThumbImage{row["thumb_image"]}
, mRemoteResource{row["remote_resource"]}
, mMediaCaption{row["media_caption"]}
, mThumbnail{[&row]() -> std::optional<jhtml::DataUri> {
    if (auto thumb{row.at<std::vector<unsigned char>>("thumbnail")}) {
        return jhtml::DataUri{"image/jpeg", *thumb};
    }
    return std::nullopt;
}()}
, mFilePath{row["file_path"]}
{
    assert(0 == mFromMe || 1 == mFromMe);
}

Message const& Message::systemMessage(jmsg::Chat& chat) const
{
    jmsg::Message::Status const systemType{
        [this]()
        {
            switch (mMediaSize) {
            case System::kE2eChanged:
            case System::kGroupE2e:
                return jmsg::Message::Status::kNotificationE2e;

            case System::kBizAccount0:
            case System::kBizAccount1:
            case System::kBizOfficial:
            case System::kBizToStandard:
                return jmsg::Message::Status::kNotificationBiz;

            default:
                return jmsg::Message::Status::kSystem;
            }
        }()};

    jmsg::Message message{chat, mId, systemType, false};

    auto const actor{mChat.lookup(mRemoteResource.value_or(mRemoteJid))};
    switch (mMediaSize) {

    case System::kCallMissed:
        message.text(
            "Missed " + std::string{mMediaCaption.value_or("audio")} +
            " call from " + actor + ".");
        break;

    case System::kSubjectChanged:
    {
        std::string text{actor + " changed the subject"};
        if (auto const object{JavaObject::read(mThumbImage)}) {
            text += " from »" + object->toString() + "«";
        }
        message.text(text + " to »" + std::string{mData.value()} + "«.");
        break;
    }

    case System::kUnknown0:
        // Only occurs in otherwise empty temporary chats.
        break;

    case System::kGroupAddedYou:
        message.text("You were added to the group.");
        break;

    case System::kGroupLeft:
        message.text(actor + " left.");
        break;

    case System::kIconChanged:
    {
        jmsg::Message::Para p{message};
        if (auto const photoChange{JavaObject::read(mThumbImage)}) {
            if (std::binary_search(
                    std::begin(kProfilePhotoChangeClassIds),
                    std::end(kProfilePhotoChangeClassIds),
                    photoChange->object.getClass()->classId()))
            {
                auto const& object{get<jois::Object>(photoChange->object)};
                makeImageTag(message, object->values.at(kOldPhotoIndex));
                makeImageTag(message, object->values.at(kNewPhotoIndex));
            } else {
                std::cerr << "Unknown ProfilePhotoChange class: "
                          << photoChange->object.getClass() << '\n';
            }
        }
        p.text(actor + " changed this group's icon.");
        break;
    }

    case System::kNumberChanged0:
        message.text(
            actor + "'s number changed to " +
            mChat.formatJid(JavaObject::read(mThumbImage).value().toString()) +
            ".");
        break;

    case System::kGroupCreated:
        message.text(
            actor + " created group " + std::string{mData.value()} + ".");
        break;

    case System::kGroupAdded:
        message.text(
            actor + " added " +
            JavaObject::read(mThumbImage).value().asPeople(mChat) + ".");
        break;

    case System::kGroupLeftMulti:
        message.text(
            JavaObject::read(mThumbImage).value().asPeople(mChat) + " left.");
        break;

    case System::kGroupRemovedMulti:
        message.text(
            actor + " removed " +
            JavaObject::read(mThumbImage).value().asPeople(mChat) + ".");
        break;

    case System::kGroupAdminGranted:
        message.text(
            actor + " made " +
            JavaObject::read(mThumbImage).value().asPeople(mChat) +
            " an admin.");
        break;

    case System::kE2eChanged:
        message.text(actor + "'s security code changed");
        break;

    case System::kGroupE2e:
        message.text(
            "Messages you send to this chat or group are secured with "
            "end-to-end encryption.");
        break;

    case System::kGroupJoined:
        message.text("You joined the group via an invite link.");
        break;

    case System::kGroupDescription:
        message.text(
            actor + " changed the group description to " +
            std::string{mData.value()} + ".");
        break;

    case System::kNumberChanged1:
        message.text(
            actor + "'s number changed to " + std::string{mMediaName.value()} +
            ".");
        break;

    case System::kGroupInfoRestricted:
        message.text(actor + " allowed only admins to edit this group's info.");
        break;

    case System::kGroupInfoUnrestricted:
        message.text(
            actor + " allowed all participants to edit this group's info.");
        break;

    case System::kGroupRestricted:
        message.text(
            actor + " allowed only admins to send messages to this group.");
        break;

    case System::kGroupUnrestricted:
        message.text(
            actor +
            " allowed all participants to send messages to this group.");
        break;

    case System::kBizAccount0:
    case System::kBizAccount1:
        message.text(
            "This chat is with a business account of »" +
            std::string{mMediaName.value()} + "«.");
        break;

    case System::kBizOfficial:
        message.text(
            "This chat is with the official business account of »" +
            std::string{mMediaName.value()} + "«.");
        break;

    case System::kBizToStandard:
        message.text(
            "The business account of »" + std::string{mMediaName.value()} +
            "« has now registered as a standard account.");
        break;

    default:
        message.text("Unknown media_size=" + std::to_string(mMediaSize) + ".");
        std::cerr << "Unknown media_size=" << mMediaSize << ".\n";
        break;
    }
    return *this;
}

Message const& Message::textMessage(jmsg::Chat& chat) const
{
    auto const status{mRow.at<int>("status")};
    assert(
        !mFromMe || Status::kSent == status || Status::kDelivered == status ||
        Status::kSentAudio == status || Status::kSeen == status);
    assert(WaType::kImage == mType || !mFromMe || !mRemoteResource);

    jmsg::Message message{
        chat,
        mId,
        !mFromMe                           ? jmsg::Message::Status::kReceived
            : Status::kSeen == status      ? jmsg::Message::Status::kSeen
            : Status::kDelivered == status ? jmsg::Message::Status::kDelivered
                                           : jmsg::Message::Status::kSent,
        0 != mRow.at<int>("forwarded").value_or(0)};
    auto const actor{mRemoteResource.value_or("")};
    auto const author{
        mFromMe ? "You" : mChat.lookup("" != actor ? actor : mRemoteJid)};
    // No author is shown for one-to-one chats where remote_resource is
    // empty.
    if (!mFromMe && "" != actor) {
        message.author(author);
    }

    auto const quoted_row_id{mRow.at<int>("quoted_row_id")};
    if (0 != quoted_row_id.value_or(0)) {
        jsql::Query quotes{mQuotes, {*quoted_row_id}};
        assert(!quotes.empty());
        auto const& quotedRow{quotes.front()};
        auto const quotedJid{quotedRow["key_remote_jid"]};
        if (!quotedJid) {
            // WhatsApp Web shows an error in this situation.
            std::cerr << "Quoted message lost: quoted_row_id=" << *quoted_row_id
                      << '\n';
        } else {
            jmsg::Message::Quote const quote{message};

            auto const quotedFromMe{1 == quotedRow.at<int>("key_from_me")};
            auto const quotedActor{quotedRow["remote_resource"].value_or("")};
            auto const quotedAuthor{
                quotedFromMe
                    ? "You"
                    : mChat.lookup(
                          "" != quotedActor ? quotedActor : quotedJid.value())};
            if (mRemoteJid == quotedJid) {
                message.author(quotedAuthor);
            } else {
                // If the quote is from another channel, show its name.
                message.author(
                    quotedAuthor + " • " + mChat.lookup(quotedJid.value()));
            }
            jwa::Message{mChat, quotedRow, mQuotes}.messageText(message);
        }
    }

    messageText(message);
    return *this;
}

Message const& Message::messageText(jmsg::Message& message) const
{
    switch (mType) {
    case WaType::kText:
        assert(!mMimeType);
        assert(mMediaCaption || !mMediaName);
        assert(mMediaCaption || !mMediaUrl);
        assert(0 == mMediaDuration.value_or(0));
        assert(!mThumbnail);
        assert(!mFilePath);
        if (mMediaCaption) {
            jmsg::Message::Quote quote{message};
            if (mMediaUrl) {
                message.link(jhtml::Uri{*mMediaUrl}, *mMediaCaption);
            } else {
                message.text(*mMediaCaption);
            }
            if (mMediaName) {
                message.text(*mMediaName);
            }
        }
        message.text(mData.value());
        break;

    case WaType::kImage:
    case WaType::kVideo:
    case WaType::kGif:
    {
        assert(!mData);
        auto const mime{mMimeType.value_or("image/jpeg")};
        jmsg::Message::Figure figure{message};
        if (mime.starts_with("image/")) {
            message.image(mime, mFilePath, mThumbnail.value());
        } else if (mime.starts_with("video/")) {
            message.video(mFilePath, mThumbnail.value());
        } else {
            assert(false);
        }
        if (mMediaCaption) {
            figure.caption(*mMediaCaption);
        }
        break;
    }

    case WaType::kSticker:
        assert(!mData);
        assert(!mMediaCaption);
        assert(!mThumbnail);
        message.sticker(mFilePath.value());
        break;

    case WaType::kAudio:
        assert(!mData);
        assert(!mMediaCaption);
        assert(!mThumbnail);
        if (mFilePath) {
            message.audio(*mFilePath);
        } else {
            message.text("[Not downloaded]");
        }
        break;

    case WaType::kVCard:
        assert(!mMimeType);
        assert(!mMediaUrl);
        assert(!mMediaCaption);
        assert(0 == mMediaDuration.value_or(0));
        assert(!mThumbnail);
        assert(!mFilePath);
        message.dataUri(
            jhtml::DataUri{"text/vcard;charset=utf-8", mData.value()},
            mMediaName.value());
        break;

    case WaType::kLocation:
    {
        assert(!mData);
        assert(!mMimeType);
        assert(!mMediaCaption);
        assert(0 == mMediaDuration.value_or(0));
        assert(!mFilePath);
        jmsg::Message::Figure figure{message};
        if (mThumbnail) {
            message.thumbnail("Location", *mThumbnail);
        }
        jmsg::Message::Caption caption{figure};
        message.text(
            "Location: " + std::to_string(mLatitude.value()) + "," +
            std::to_string(mLongitude.value()) + ".");
        assert(mMediaName || !mMediaUrl);
        if (mMediaName) {
            if (mMediaUrl) {
                message.link(jhtml::Uri{*mMediaUrl}, *mMediaName);
            } else {
                message.text(*mMediaName);
            }
        }
        break;
    }

    case WaType::kFile:
    {
        assert(!mData);
        std::optional<jmsg::Message::Figure> figure{};
        std::optional<jmsg::Message::Caption> caption{};
        if (mThumbnail) {
            figure.emplace(message);
            message.thumbnail(mMediaCaption.value(), *mThumbnail);
            caption.emplace(*figure);
        }
        if (mFilePath) {
            message.link(jhtml::Uri{*mFilePath}, mMediaCaption.value());
        } else {
            message.text("[Not downloaded]");
            message.text(mMediaCaption.value());
        }
        break;
    }

    // Only system messages about missed audio/video calls.
    case WaType::kCallMissed:
        assert(false);
        break;

    case WaType::kDeleted:
        assert("" == mData.value_or(""));
        assert(!mMimeType);
        assert(!mMediaUrl);
        assert(0 == mMediaDuration.value_or(0));
        assert(!mMediaCaption);
        assert(!mThumbnail);
        message.note("🚫 This message was deleted.");
        break;

    case WaType::kLocationLive:
    {
        assert(!mData);
        assert(!mMimeType);
        assert(!mMediaUrl);
        assert(!mFilePath);
        jmsg::Message::Figure figure{message};
        if (mThumbnail) {
            message.thumbnail("Live location", *mThumbnail);
        }
        figure.caption(
            "Live location " +
            ("" != mMediaCaption.value_or("") ? "»" + *mMediaCaption + "« "
                                              : "") +
            "at " + std::to_string(mLatitude.value()) + "," +
            std::to_string(mLongitude.value()) + " for " +
            std::to_string(mMediaDuration.value()) + "s.");
        break;
    }

    case WaType::kGroupInvite:
        // Group invitation.
        assert(!mData);
        assert(!mMimeType);
        assert(!mMediaUrl);
        assert(!mMediaName);
        assert(0 == mMediaDuration.value_or(0));
        assert(!mThumbnail);
        assert(!mFilePath);
        message.text(mMediaCaption.value());
        break;

    default:
        message.text("Unknown media_wa_type=" + std::to_string(mType));
        std::cerr << "Unknown media_wa_type=" << std::to_string(mType) << '\n';
        break;
    }
    return *this;
}

jmsg::Chat& operator<<(jmsg::Chat& chat, Message const& message)
{
    assert(WaType::kText != message.mType || !message.mFilePath);

    system_clock::time_point const timestamp{
        kWhatsAppEpoch +
        milliseconds{message.mRow.at<milliseconds::rep>("timestamp").value()}};
    chat.timestamp(timestamp);

    // status=6 encodes system messages using media_size.
    auto const status{message.mRow.at<int>("status")};
    if (Status::kSystem == status) {
        message.systemMessage(chat);
    } else {
        message.textMessage(chat);
    }
    return chat;
}

} // namespace jwa
