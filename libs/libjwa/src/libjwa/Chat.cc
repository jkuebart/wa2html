#include "libjwa/Chat.h"

#include "date/date.h"
#include "libjmsg/ChatIndex.h"
#include "libjsql/SimpleQuery.h"

#include <algorithm>
#include <chrono>

using date::sys_days;
using date::year;
using date::year_month_day;
using std::chrono::milliseconds;
using std::chrono::system_clock;

namespace
{

/// WhatsApp time stamps are relative to this date.
constexpr sys_days kWhatsAppEpoch{year_month_day{year{1970} / 1 / 1}};

} // namespace

namespace jwa
{

std::string Chat::formatJid(std::string_view const& jid)
{
    auto const tail{std::max(std::string::size_type{15}, jid.size()) - 15};
    return "@s.whatsapp.net" == jid.substr(tail)
        ? "+" + std::string{jid.substr(0, tail)}
        : std::string{jid};
}

Chat::Chat(
    std::unordered_map<std::string, std::string> const& jids,
    jsql::Query const& row,
    jsql::Statement& messages,
    jsql::Statement& quotes,
    jsql::Statement& groupParticipants)
: mJids{jids}
, mRow{row}
, mJid{row["raw_string_jid"].value()}
, mMessages{messages, {mJid}}
, mQuotes{quotes}
, mParticipants{groupParticipants}
{}

std::string Chat::displayName() const
{
    std::string const subject{mRow["subject"].value_or("")};
    if ("" != subject) {
        return subject;
    }
    return lookup(mJid);
}

std::string Chat::filename() const
{
    return mJid + ".html";
}

bool Chat::empty() const
{
    return mMessages.empty();
}

Message Chat::front() const
{
    return Message{*this, mMessages.front(), mQuotes};
}

void Chat::popFront()
{
    mMessages.popFront();
}

std::string const& Chat::jid() const noexcept
{
    return mJid;
}

std::string Chat::lookup(std::string const& jid) const
{
    auto const it{mJids.find(jid)};
    return mJids.end() != it ? it->second : formatJid(jid);
}

jsql::Query const& Chat::row() const noexcept
{
    return mRow;
}

std::vector<std::string> Chat::groupParticipants() const
{
    std::vector<std::string> result{};
    jsql::Query participants{mParticipants, {mRow["raw_string_jid"].value()}};
    for (; !participants.empty(); participants.popFront()) {
        auto const& participant{participants.front()};
        std::string const jid{participant["jid"].value()};
        result.emplace_back("" == jid ? "You" : lookup(jid));
    }
    std::sort(result.begin(), result.end());
    return result;
}

jmsg::ChatIndex& operator<<(jmsg::ChatIndex& chatIndex, Chat const& chat)
{
    auto const& row{chat.row()};

    std::optional<system_clock::time_point> timestamp{};
    if (auto const sort_timestamp{row.at<milliseconds::rep>("sort_timestamp")})
    {
        timestamp = kWhatsAppEpoch + milliseconds{*sort_timestamp};
    }

    return chatIndex.chat(
        chat.jid(),
        chat.filename(),
        chat.displayName(),
        timestamp,
        row["display_message"].value_or(""),
        row.at<unsigned>("unseen_message_count").value_or(0),
        row.at<int>("last_read_message_row_id"),
        row.at<int>("is_group").value() ? jmsg::ChatIndex::Type::kGroup
                                        : jmsg::ChatIndex::Type::kUser);
}

} // namespace jwa
