#include "libjwa/Chats.h"

#include "libjwa/Chat.h"

namespace
{

constexpr char const* kChatQuery{R"(
    select
        c.subject,
        c.last_read_message_row_id,
        c.sort_timestamp,
        c.raw_string_jid,
        c.unseen_message_count,
        exists(
            select
                *
            from group_participants g
            where
                c.raw_string_jid = g.gjid
        ) as is_group,
        m.data as display_message

    from chat_view c
    left join messages m on
        c.raw_string_jid = m.key_remote_jid and
        c.display_message_row_id = m._id

    order by
        c.sort_timestamp desc
    ;
)"};

constexpr char const* kMessageQuery{R"(
    select
        m._id,
        m.key_remote_jid,
        m.key_from_me,
        m.status,
        m.data,
        m.timestamp,
        m.media_mime_type,
        m.media_wa_type,
        m.media_size,
        m.media_url,
        m.media_name,
        m.media_duration,
        m.latitude,
        m.longitude,
        m.thumb_image,
        m.remote_resource,
        m.media_caption,
        m.quoted_row_id,
        m.forwarded,
        coalesce(mt.thumbnail, m.raw_data) as thumbnail,
        mm.file_path

    from messages m
    left join message_thumbnails mt using
        (key_remote_jid, key_from_me, key_id)
    left join message_media mm on
        m.media_hash == mm.file_hash and
        m._id = mm.message_row_id

    where
        ? = m.key_remote_jid

    order by
        m.key_remote_jid,
        m.timestamp,
        m._id
    ;
)"};

constexpr char const* kQuotesQuery{R"(
    select
        m._id,
        m.key_remote_jid,
        m.key_from_me,
        m.data,
        m.media_mime_type,
        m.media_wa_type,
        m.media_size,
        m.media_url,
        m.media_name,
        m.media_duration,
        m.latitude,
        m.longitude,
        m.thumb_image,
        m.remote_resource,
        m.media_caption,
        coalesce(mt.thumbnail, m.raw_data) as thumbnail,
        mm.file_path
    from messages_quotes q
    left join messages m using
        (key_remote_jid, key_from_me, key_id)
    left join message_thumbnails mt using
        (key_remote_jid, key_from_me, key_id)
    left join message_media mm on
        m.media_hash = mm.file_hash and
        m._id = mm.message_row_id
    where
        ? = q._id
    ;
)"};

constexpr char const* kParticipantsQuery{R"(
    select * from group_participants where ? = gjid;
)"};

} // namespace

namespace jwa
{

Chats::Chats(jsql::Db const& msgstore)
: mChats{msgstore, kChatQuery}
, mMessages{msgstore, kMessageQuery}
, mQuotes{msgstore, kQuotesQuery}
, mParticipants{msgstore, kParticipantsQuery}
, mJids{}
{}

Chats::Chats(jsql::Db const& msgstore, jsql::Db const& wa) : Chats(msgstore)
{
    jsql::SimpleQuery wa_contacts{
        wa,
        R"(
            select
                jid,
                coalesce(display_name, wa_name) as display_name
            from wa_contacts;
        )"};
    for (; !wa_contacts.empty(); wa_contacts.popFront()) {
        auto const& row{wa_contacts.front()};
        if (auto const display_name{row["display_name"]}) {
            mJids.emplace(row["jid"].value(), *display_name);
        }
    }
}

bool Chats::empty() const
{
    return mChats.empty();
}

Chat Chats::front()
{
    return Chat{mJids, mChats.front(), mMessages, mQuotes, mParticipants};
}

void Chats::popFront()
{
    mChats.popFront();
}

} // namespace jwa
