#include "libjhtml/Link.h"

namespace jhtml
{
class Document;

Link::Link(
    Document& document,
    std::string_view const& relation,
    std::string_view const& href)
: mElement{document, "link", {{"rel", relation}, {"href", href}}}
{}

} // namespace jhtml
