#include "libjhtml/Time.h"

#include <ctime>
#include <iomanip>
#include <sstream>

using std::chrono::system_clock;

namespace
{

/// ISO 8601 date-time format.
constexpr char const* kIso8601DateTime{"%FT%T%z"};

/// Replacement for std::format() until it becomes available.
std::string format(char const* fmt, system_clock::time_point const& time)
{
    std::time_t const t{system_clock::to_time_t(time)};
    std::ostringstream str{};
    str << std::put_time(std::localtime(&t), fmt);
    return str.str();
}

} // namespace

namespace jhtml
{

Time::Time(
    Document& document,
    char const* const fmt,
    system_clock::time_point const& time)
: mElement{
      document,
      "time",
      {{"datetime", format(kIso8601DateTime, time)},
       {"title", format(kIso8601DateTime, time)}},
      format(fmt, time)}
{}

} // namespace jhtml
