#include "libjhtml/Element.h"

#include "libjhtml/Document.h"

namespace jhtml
{

Element::Element(
    Document& document,
    std::string_view const& name,
    Attributes const& attributes,
    std::string_view const& text)
: mDocument{document}
, mName{name}
{
    Document::StartTag{document, name, attributes};
    document.text(text);
}

Element::~Element()
{
    Document::EndTag{mDocument, mName};
}

} // namespace jhtml
