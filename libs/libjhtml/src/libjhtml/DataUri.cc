#include "libjhtml/DataUri.h"

#include "libjhtml/Uri.h"

#include <ostream>

namespace
{

constexpr std::string_view kBase64{
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"};

constexpr unsigned char kZero{0};

} // namespace

namespace jhtml
{

/// @see https://tools.ietf.org/html/rfc4648#section-4
std::ostream& operator<<(std::ostream& os, DataUri const& dataUri)
{
    os << "data:" << Uri{dataUri.mimeType};
    if (std::holds_alternative<std::string>(dataUri.data)) {
        return os << ',' << Uri{std::get<std::string>(dataUri.data)};
    }

    os << ";base64,";
    auto const& data{std::get<std::vector<unsigned char>>(dataUri.data)};
    for (auto pos{data.begin()}, end{data.end()}; pos != end; ++pos) {
        unsigned char chr{*pos};
        os << kBase64[chr / 4];
        chr = 16 * (chr % 4);
        ++pos;
        unsigned char const c0{pos != end ? *pos : kZero};
        chr += c0 / 16;
        os << kBase64[chr];
        if (pos == end) {
            os << "==";
            break;
        }
        chr = 4 * (c0 % 16);
        ++pos;
        unsigned char const c1{pos != end ? *pos : kZero};
        chr += c1 / 64;
        os << kBase64[chr];
        if (pos == end) {
            os << "=";
            break;
        }
        chr = c1 % 64;
        os << kBase64[chr];
    }
    return os;
}

} // namespace jhtml
