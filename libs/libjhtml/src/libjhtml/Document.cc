#include "libjhtml/Document.h"

#include "libjhtml/DataUri.h"
#include "libjhtml/Uri.h"

#include <algorithm>
#include <iomanip>
#include <iterator>
#include <sstream>
#include <string>
#include <utility>
#include <variant>

namespace
{

/// @see https://html.spec.whatwg.org/multipage/syntax.html#elements-2.
bool voidElement(std::string_view const& name)
{
    static std::string_view const isVoid[]{
        "area",
        "base",
        "br",
        "col",
        "embed",
        "hr",
        "img",
        "input",
        "link",
        "meta",
        "param",
        "source",
        "track",
        "wbr"};
    return std::binary_search(std::begin(isVoid), std::end(isVoid), name);
}

enum class Escape
{
    kAttribute,
    kText
};

/// @see https://html.spec.whatwg.org/multipage/parsing.html#escapingString.
struct HtmlEscaped
{
    std::string_view const string;
    Escape const quote;
};

std::ostream& operator<<(std::ostream& os, HtmlEscaped const& escaped)
{
    std::string_view::size_type pos{0};
    for (;;) {
        auto const next{escaped.string.find_first_of(
            Escape::kText == escaped.quote ? "&<>" : "\"&",
            pos)};
        if (std::string_view::npos == next) {
            break;
        }
        os << escaped.string.substr(pos, next - pos);
        switch (escaped.string[next]) {
        case '"':
            os << "&quot;";
            break;
        case '&':
            os << "&amp;";
            break;
        case '<':
            os << "&lt;";
            break;
        case '>':
            os << "&gt;";
            break;
        }
        pos = 1 + next;
    }
    return os << escaped.string.substr(pos);
}

} // namespace

namespace jhtml
{

Document::Document(std::ostream& os) : mOs{os}, mIndent{0}
{
    os << "<!doctype html\n>";
}

Document::~Document()
{
    mOs << '\n';
}

Document& Document::text(std::string_view const& string)
{
    mOs << HtmlEscaped{string, Escape::kText};
    return *this;
}

Document::StartTag::StartTag(
    Document& document,
    std::string_view const& name,
    Attributes const& attributes)
: mDocument{document}
{
    if (!voidElement(name)) {
        ++document.mIndent;
    }
    document.mOs << '<' << name;
    for (auto const& attr : attributes) {
        std::visit(
            [this, &attr](auto const& value)
            {
                attribute(attr.first, value);
            },
            attr.second);
    }
}

Document::StartTag::~StartTag()
{
    mDocument.mOs << '\n' << std::setw(1 + 2 * mDocument.mIndent) << '>';
}

Document::StartTag& Document::StartTag::attribute(std::string_view const& name)
{
    mDocument.mOs << ' ' << name;
    return *this;
}

Document::StartTag& Document::StartTag::attribute(
    std::string_view const& name,
    std::string_view const& value)
{
    attribute(name);
    if (name != value) {
        mDocument.mOs << "=\"" << HtmlEscaped{value, Escape::kAttribute} << '"';
    }
    return *this;
}

Document::StartTag& Document::StartTag::attribute(
    std::string_view const& name,
    Uri const& uri)
{
    // Having to produce a std::string after all is unsatisfactory.
    std::ostringstream value{};
    value << uri;
    return attribute(name, value.str());
}

Document::StartTag& Document::StartTag::attribute(
    std::string_view const& name,
    DataUri const& dataUri)
{
    // Having to produce a std::string after all is unsatisfactory.
    std::ostringstream value{};
    value << dataUri;
    return attribute(name, value.str());
}

Document::EndTag::EndTag(Document& document, std::string_view const& name)
{
    if (!voidElement(name)) {
        --document.mIndent;
        document.mOs << "</" << name << '\n'
                     << std::setw(1 + 2 * document.mIndent) << '>';
    }
}

} // namespace jhtml
