#include "libjhtml/Meta.h"

namespace jhtml
{
class Document;

Meta::Meta(
    Document& document,
    std::string_view const& name,
    std::string_view const& content)
: mElement{document, "meta", {{"name", name}, {"content", content}}}
{}

} // namespace jhtml
