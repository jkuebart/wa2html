#include "libjhtml/Uri.h"

#include <algorithm>
#include <cctype>
#include <iomanip>
#include <iterator>
#include <ostream>

namespace
{

constexpr std::string_view kUriChars{"!#$&'()*+,-./:;=?@[]_~"};

bool invalidChar(char const c)
{
    // Because
    //   - std::string is hardwired to "char" which may be signed or unsigned,
    //     and
    //   - std::isalnum(int) expects an int because it must accept EOF,
    // we must cast to "unsigned char" in order to adhere to the correct value
    // range in all cases.
    return !std::binary_search(kUriChars.begin(), kUriChars.end(), c) &&
        !std::isalnum(static_cast<unsigned char>(c));
}

} // namespace

namespace jhtml
{

std::ostream& operator<<(std::ostream& os, Uri const& uri)
{
    auto pos{uri.uri.begin()};
    while (uri.uri.end() != pos) {
        auto const next{std::find_if(pos, uri.uri.end(), invalidChar)};
        std::copy(pos, next, std::ostream_iterator<char>(os));
        pos = next;
        if (pos != uri.uri.end()) {
            os << '%' << std::hex << std::setfill('0') << std::setw(2)
               << static_cast<int>(*pos) << std::dec << std::setfill(' ');
            ++pos;
        }
    }
    return os;
}

} // namespace jhtml
