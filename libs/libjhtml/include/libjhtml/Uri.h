#ifndef LIBJHTML_URI_H
#define LIBJHTML_URI_H

#include <iosfwd>
#include <string_view>

namespace jhtml
{

struct Uri
{
    std::string_view const uri;
};

/// Outputs the URI with invalid characters percent encoded as per RFC3986.
///
/// @param os An output stream.
/// @param uri The URI.
/// @return The output stream.
std::ostream& operator<<(std::ostream& os, Uri const& uri);

} // namespace jhtml

#endif // LIBJHTML_URI_H
