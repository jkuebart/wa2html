#ifndef LIBJHTML_META_H
#define LIBJHTML_META_H

#include "libjhtml/Element.h"

#include <string_view>

namespace jhtml
{
class Document;

class Meta
{
public:
    /// @param document The document into which to insert the element.
    /// @param name The meta element's name attribute.
    /// @param content The meta element's content attribute.
    Meta(
        Document& document,
        std::string_view const& name,
        std::string_view const& content);

private:
    Element mElement;
};

} // namespace jhtml

#endif // LIBJHTML_META_H
