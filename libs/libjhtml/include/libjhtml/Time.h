#ifndef LIBJHTML_TIME_H
#define LIBJHTML_TIME_H

#include "libjhtml/Element.h"

#include <chrono>

namespace jhtml
{
class Document;

class Time
{
public:
    /// @param document The document into which to insert the element.
    /// @param fmt The format string for the element's text content.
    /// @param time The time represented by the new element.
    Time(
        Document& document,
        char const* fmt,
        std::chrono::system_clock::time_point const& time);

private:
    Element mElement;
};

} // namespace jhtml

#endif // LIBJHTML_TIME_H
