#ifndef LIBJHTML_DATAURI_H
#define LIBJHTML_DATAURI_H

#include <iosfwd>
#include <string>
#include <variant>
#include <vector>

namespace jhtml
{

/// The data URI can contain text or binary data. The latter is
/// automatically encoded as base64 according to RFC2397.
///
/// @see https://tools.ietf.org/html/rfc2397
struct DataUri
{
    std::string const mimeType;
    std::variant<std::string, std::vector<unsigned char>> const data;
};

/// @param os An output stream.
/// @param dataUri The URI.
/// @return The output stream.
std::ostream& operator<<(std::ostream& os, DataUri const& dataUri);

} // namespace jhtml

#endif // LIBJHTML_DATAURI_H
