#ifndef LIBJHTML_ELEMENT_H
#define LIBJHTML_ELEMENT_H

#include "libjhtml/Attributes.h"

#include <string>
#include <string_view>

namespace jhtml
{

class Document;

/// An RAII class that adds an element to a HTML document.
class Element
{
public:
    /// @param document The document into which to insert the element.
    /// @param name The element name.
    /// @param attributes Attributes for the element's start tag.
    /// @param text The element's text contents.
    Element(
        Document& document,
        std::string_view const& name,
        Attributes const& attributes = {},
        std::string_view const& text = {});
    ~Element();

    Element(Element const&) = delete;

private:
    Document& mDocument;
    std::string const mName;
};

} // namespace jhtml

#endif // LIBJHTML_ELEMENT_H
