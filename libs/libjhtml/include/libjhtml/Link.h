#ifndef LIBJHTML_LINK_H
#define LIBJHTML_LINK_H

#include "libjhtml/Element.h"

#include <string_view>

namespace jhtml
{
class Document;

class Link
{
public:
    /// @param document The document into which to insert the element.
    /// @param relation The link's relation.
    /// @param href The link's target URI.
    Link(
        Document& document,
        std::string_view const& relation,
        std::string_view const& href);

private:
    Element mElement;
};

} // namespace jhtml

#endif // LIBJHTML_LINK_H
