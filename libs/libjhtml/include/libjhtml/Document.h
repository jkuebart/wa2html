#ifndef LIBJHTML_DOCUMENT_H
#define LIBJHTML_DOCUMENT_H

#include "libjhtml/Attributes.h"

#include <iosfwd>
#include <string_view>

namespace jhtml
{
struct DataUri;
struct Uri;

/// A builder for a HTML document.
class Document
{
public:
    class StartTag
    {
    public:
        /// @param document The document.
        /// @param name The element name.
        /// @param attributes The attributes for the start tag.
        StartTag(
            Document& document,
            std::string_view const& name,
            Attributes const& attributes = {});
        ~StartTag();

        StartTag& attribute(std::string_view const& name);
        StartTag& attribute(
            std::string_view const& name,
            std::string_view const& value);
        StartTag& attribute(std::string_view const& name, Uri const& uri);
        StartTag& attribute(
            std::string_view const& name,
            DataUri const& dataUri);

    private:
        Document& mDocument;
    };

    class EndTag
    {
    public:
        /// @param document The document.
        /// @param name The element name.
        EndTag(Document& document, std::string_view const& name);
    };

    explicit Document(std::ostream& os);
    ~Document();

    /// @param string The text to be inserted into the document.
    /// @return The document.
    Document& text(std::string_view const& string);

private:
    std::ostream& mOs;
    int mIndent;
};

} // namespace jhtml

#endif // LIBJHTML_DOCUMENT_H
