#ifndef LIBJHTML_ATTRIBUTES_H
#define LIBJHTML_ATTRIBUTES_H

#include "libjhtml/DataUri.h"
#include "libjhtml/Uri.h"

#include <string_view>
#include <unordered_map>
#include <variant>

namespace jhtml
{

using Attributes = std::unordered_map<
    std::string_view,
    std::variant<std::string_view, Uri, DataUri>>;

}

#endif // LIBJHTML_ATTRIBUTES_H
