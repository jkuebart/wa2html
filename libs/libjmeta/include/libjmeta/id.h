#ifndef LIBJMETA_ID_H
#define LIBJMETA_ID_H

namespace jmeta
{

template<typename T>
struct id
{
    using type = T;
};

template<typename T>
using e = typename T::type;

} // namespace jmeta

#endif // LIBJMETA_ID_H
