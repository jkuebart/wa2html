#ifndef LIBJMETA_TRANSFORM_H
#define LIBJMETA_TRANSFORM_H

#include "libjmeta/id.h"

namespace jmeta
{

namespace detail
{

    template<template<typename...> typename F, typename L>
    struct transform;

    template<
        template<typename...>
        typename F,
        template<typename...>
        typename L,
        typename... Ts>
    struct transform<F, L<Ts...>> : id<L<F<Ts>...>>
    {};

} // namespace detail

template<template<typename...> typename F, typename L>
using transform = e<detail::transform<F, L>>;

} // namespace jmeta

#endif // LIBJMETA_TRANSFORM_H
