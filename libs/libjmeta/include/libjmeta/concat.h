#ifndef LIBJMETA_CONCAT_H
#define LIBJMETA_CONCAT_H

#include "libjmeta/id.h"

namespace jmeta
{

namespace detail
{

    template<typename... Ls>
    struct concat;

    template<typename L>
    struct concat<L> : id<L>
    {};

    template<
        template<typename...>
        typename L,
        typename... Ts,
        typename... Us,
        typename... Ls>
    struct concat<L<Ts...>, L<Us...>, Ls...> : concat<L<Ts..., Us...>, Ls...>
    {};

} // namespace detail

template<typename... Ls>
using concat = e<detail::concat<Ls...>>;

} // namespace jmeta

#endif // LIBJMETA_CONCAT_H
