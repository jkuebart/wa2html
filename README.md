WhatsApp to HTML
================

This tool reads Android WhatsApp databases and generates a HTML archive of
conversations. It was developed for WhatsApp version 2.20.193.9 and may or
may not work for databases of other versions.

The tool expects the file names of two WhatsApp databases, `msgstore.db`
and `wa.db`. The second database is optional, it contains names from the
phone's contacts.

The tool produces a HTML file for each conversation and an `index.html` in
the directory where it is invoked. The files `group.svg`, `user.svg` and
`wa2html.css` from `/usr/local/share/wa2html/` should be copied to the same
directory:

    $ cp /usr/local/share/wa2html/* .

The folders `/sdcard/WhatsApp/Media` and `/data/data/com.whatsapp/Avatars`
from the phone should also be copied to the same directory in order to see
the images and avatars, respectively.

Timestamps are rendered according to the current local time zone, so make
sure to configure it appropriately before running.


How to get the databases
------------------------

The databases can be retrieved from a rooted phone using the commands

    $ adb root
    $ adb pull /data/data/com.whatsapp/databases

The avatars can be retrieved using

    $ adb pull /data/data/com.whatsapp/files/Avatars

The script [`tools/dl`][DL] can perform this task.

If the phone isn't rooted, it may help to install WhatsApp in an Android
emulator and restore the Google Drive backup. Then the databases can then
be obtained as shown above. There's a detailed tutorial at
[WhatsAppMigrationTools][WAMT].

Another option is to decrypt a database backup stored on the device or
downloaded from Google Drive using `tools/wagddl`. There are
[tools][CRYPT12] out there claiming to be able to do this, but I have never
tried it. However, the [decryption key must be fetched from the WhatsApp
server][ELCOM] or from a rooted device, so this method is no easier than
extracting the databases from a rooted phone directly.


Build and installation
----------------------

Building `wa2html` requires a C++ compiler and [Meson][MESON] to be
available. To build and install `wa2html` into `/usr/local`:

    $ meson setup --buildtype release build
    $ meson install -C build

To install to a different location, specify a `--prefix` in the first
command. See `meson setup --help` for more information.


Development
-----------

Tests can be run using the target `test`:

    $ meson test -C build

Some tests require the databases `libs/wa2html/data/msgstore.db` and
`libs/wa2html/data/wa.db` which are not included in this repository.

The debug version is built like this:

    $ meson setup --buildtype debug build
    $ meson compile -C build

If installed, the analyzers [scan-build][SB] and [clang-tidy][CT] can be
run using

    $ ninja -C build scan-build
    $ ninja -C build clang-tidy

On macOS with llvm installed from [homebrew][HB], clang-tidy must be run
like this:

    $ (
        PATH=/usr/local/opt/llvm/bin:/usr/local/opt/llvm/share/clang:$PATH
        CC=/usr/local/opt/llvm/bin/clang \
        CXX=/usr/local/opt/llvm/bin/clang++ \
        meson setup build-tidy
        ninja -C build-tidy clang-tidy
    )

In order to check the source code using [include-what-you-use][IWYU] on
Darwin, use

    $ iwyu_tool.py -p build -- -isysroot "$(xcrun --show-sdk-path)"


Unfinished
----------

A few things would be nice to have, but are unimplemented:

  - Group conversations should show the description.
  - Group participants should be coloured differently.
  - Message status (sent, delivered, seen) is present in the HTML but not
    visualised.
  - »Forwarded« marker is present in the HTML but not visualised.
  - No map is shown for (live) locations.
  - Message locations should be shown.
  - There should be tests using a synthetic database which contains an
    instance of all observed cases.
  - Full-text search would be nice.
  - Java's modified UTF-8 should be decoded by replacing surrogate pairs.

Because WhatsApp databases contain extremely sensitive information, I have
only used this tool with my own database of about 25,000 messages.


Further reading
---------------

  - [Extract and Decrypt Android WhatsApp Backups from Google Account][ELCOM]
  - [How to Decrypt WhatsApp crypt12 Database Messages](https://stackpointer.io/security/decrypt-whatsapp-crypt12-database-messages/559/)
  - [WhatsAppIphoneToAndroid](https://github.com/Kethen/WhatsAppIphoneToAndroid)
  - [WhatsAppMigrationTools](https://github.com/ferferga/WhatsAppMigrationTools)
  - [WhatsApp_DB_Merger](https://github.com/isabekov/WhatsApp_DB_Merger)
  - [Whatsapp_Tutorial](https://github.com/Phlop/Whatsapp_Tutorial)
  - [whapa](https://github.com/B16f00t/whapa)
  - [whatsapp-crypt12][CRYPT12]
  - [whatsapp-viewer](https://github.com/andreas-mausch/whatsapp-viewer)


Licence
-------

This program is released under a 3-clause BSD licence, see [LICENCE][].


[CRYPT12]: https://gitlab.com/stackpointer/whatsapp-crypt12
[CT]: https://clang.llvm.org/extra/clang-tidy/
[DL]: tools/dl
[ELCOM]: https://blog.elcomsoft.com/2018/01/extract-and-decrypt-whatsapp-backups-from-google/
[HB]: https://brew.sh/
[IWYU]: https://include-what-you-use.org/
[LICENCE]: LICENCE
[MESON]: https://mesonbuild.com/
[SB]: https://clang-analyzer.llvm.org/scan-build.html
[WAMT]: https://github.com/ferferga/WhatsAppMigrationTools/blob/master/merger/README.md
